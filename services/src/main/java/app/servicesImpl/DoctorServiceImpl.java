package app.servicesImpl;

import app.RandomString;
import app.dao.DoctorRepository;
import app.dto.Token;
import app.entities.Customer;
import app.entities.Doctor;
import app.servicesInterfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Transactional
@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorContext doctorContext;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public Doctor save(Doctor doctor){
        return doctorRepository.save(doctor);
    }

    @Override
    public Doctor update(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    @Override
    public void delete(Doctor doctor) {
        doctor.setAddress(null);
        doctorRepository.delete(doctor);
    }

    @Override
    public void deleteById(Long id) {
        doctorRepository.deleteById(id);
    }

    @Override
    public Long count() {
        return doctorRepository.count();
    }

    @Override
    public Doctor findByName(String name) {
        return doctorRepository.findByName(name);
    }

    @Override
    public Optional<Doctor> getById(Long id) {
        return doctorRepository.getById(id);
    }

    @Override
    public Page<Doctor> findAllByName(String name, Pageable p) {
        return doctorRepository.findAllByName(name, p);
    }

    @Override
    public Optional<Doctor> findByEmail(String email) {
        return doctorRepository.findByEmail(email);
    }

    @Override
    public Page<Doctor> findAllByNameContaining(String name, Pageable p) {
        return doctorRepository.findAllByNameContaining(name, p);
    }

    @Override
    public List<Doctor> findAll(){
        return doctorRepository.findAll();
    }

    @Override
    public List<Doctor> findAllByAddress_CityNameAndSpecialization(String cityName, String specialization) {
        return doctorRepository.findAllByAddress_CityNameAndSpecialization(cityName, specialization);
    }


    @Override
    public Doctor checkDoctorToken(Token token) {
        Optional<Doctor> userDB = doctorRepository.findByToken(token.getToken());

        if(userDB.isPresent()) {
            return userDB.get();
        } else {
            return null;
        }
    }


    @Override
    public void addNewDoctor(Doctor doctor) {
        doctor.setToken(UUID.randomUUID().toString());
        String password = doctor.getPassword();
        doctor.setPassword(passwordEncoder.encode(doctor.getPassword()));
        doctorRepository.save(doctor);
    }


    @Override
    public Token login(Doctor doctor) {
        Optional<Doctor> userDB = doctorRepository.findByEmail(doctor.getEmail());

        if(!userDB.isPresent()) {
            return null; // nie ma takiego usera
        } else if (passwordEncoder.matches(doctor.getPassword(), userDB.get().getPassword())) {
            Token token = new Token();
            token.setToken(userDB.get().getToken());
            return token;
        } else { // bledne haslo
            return null;
        }
    }

    @Override
    public void logout() {
        Optional<Doctor> userDB = doctorRepository.findByEmail(doctorContext.getDoctor().getEmail());
        if(!userDB.isPresent()) {
            return;
        }
        userDB.get().setToken(UUID.randomUUID().toString());
    }
}
