package app.servicesImpl;

import app.RandomString;
import app.dao.CustomerRepository;
import app.dto.Token;
import app.entities.Customer;
import app.servicesInterfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Transactional
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerContext customerContext;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;



    @Override
    public Customer update(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    @Override
    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }

    @Override
    public Long count() {
        return customerRepository.count();
    }

    @Override
    public ArrayList<Customer> findAllByFirstName(String firstName) {
        return customerRepository.findAllByFirstName(firstName);
    }

    @Override
    public ArrayList<Customer> findAllByFirstNameContaining(String firstName) {
        return customerRepository.findAllByFirstNameContaining(firstName);
    }

    @Override
    public Page<Customer> findAllByFirstName(String firstName, Pageable p) {
        return customerRepository.findAllByFirstName(firstName, p);
    }

    @Override
    public Page<Customer> findAllByFirstNameContaining(String firstName, Pageable p) {
        return customerRepository.findAllByFirstNameContaining(firstName, p);
    }

    @Override
    public Optional<Customer> getById(Long id) {
        return customerRepository.getById(id);
    }

    @Override
    public Boolean existsByFirstName(String firstName) {
        return customerRepository.existsByFirstName(firstName);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return customerRepository.existsByFirstName(email);
    }


    @Override
    public Optional<Customer> findByEmail(String email) {
        return customerRepository.findByEmail(email);
    }

    // ------------------

    @Override
    public Customer checkCustomerToken(Token token) {
        Optional<Customer> userDB = customerRepository.findByToken(token.getToken());

        if(userDB.isPresent()) {
            return userDB.get();
        } else {
            return null;
        }
    }

    @Override
    public void addNewCustomer(Customer customer) {
        RandomString gen = new RandomString(8, ThreadLocalRandom.current());


        customer.setToken(UUID.randomUUID().toString());
        String password = customer.getPassword();
        customer.setActivateCode(Integer.toString(ThreadLocalRandom.current().nextInt(100000, 100000000 + 1)));
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));
        customerRepository.save(customer);
    }


    public boolean activateAccount(String activateCode){
        Optional<Customer> userDB = customerRepository.findByActivateCode(activateCode);

        if(!userDB.isPresent()){
            return false;
        }else{
            Customer customer = userDB.get();
            customer.setIfActive(true);
            return true;
        }
    }


    @Override
    public Token login(Customer customer) {
        Optional<Customer> userDB = customerRepository.findByEmail(customer.getEmail());

        if(!userDB.isPresent()) {
            return null; // nie ma takiego usera
        } else if (passwordEncoder.matches(customer.getPassword(), userDB.get().getPassword())) {
            Token token = new Token();
            token.setToken(userDB.get().getToken());
            return token;
        } else { // bledne haslo
            return null;
        }
    }

    @Override
    public void logout() {
        Optional<Customer> userDB = customerRepository.findByEmail(customerContext.getCustomer().getEmail());
        if(!userDB.isPresent()) {
            return;
        }
        userDB.get().setToken(UUID.randomUUID().toString());
    }
}
