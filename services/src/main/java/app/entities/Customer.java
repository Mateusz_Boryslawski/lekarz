package app.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Collection;


@Data
@Entity
public class Customer {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Email
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String token;
    @Column
    private String activateCode;
    @Column
    private Boolean ifActive = false;



    public Customer() {}

    public Customer(String firstName) {
        this.firstName = firstName;
    }
}

