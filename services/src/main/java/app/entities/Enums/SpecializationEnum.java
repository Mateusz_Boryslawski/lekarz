package app.entities.Enums;

/**
 * Created by Michał on 2018-02-26.
 */

public enum SpecializationEnum {
    KARDIOLOG("Kardiolog"),
    CHIRURG("Chirurg"),
    LARYNGOLOG("Laryngolog"),
    OKULISTA("Okulista"),
    PULMONOLOG("Pulmonolog"),
    PEDIATRA("Pediatra"),
    NEUROLOG("Neurolog"),
    DERMATOLOG("Dermatolog"),
    STOMATOLOG("Stomatolog"),
    PSYCHOLOG("Psycholog"),
    PSYCHIATRA("Psychiatra"),
    SEKSUOLOG("Seksuolog"),
    GINEKOLOG("Ginekolog"),
    UROLOG("Urolog");

    private final String name;

    public String getSpecializationName() {
        return name;
    }

    SpecializationEnum(String name) {
        this.name = name;

    }
}




