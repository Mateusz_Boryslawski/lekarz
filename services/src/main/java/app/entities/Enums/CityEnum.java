package app.entities.Enums;

/**
 * Created by Michał on 2018-02-26.
 */
public enum CityEnum {
    WROCLAW("Wrocław"),
    LODZ("Łódź"),
    KRAKOW("Kraków"),
    WARSZAWA("Warszawa"),
    GDANSK("Gdańsk"),
    POZNAN("Poznań"),
    KATOWICE("Katowice"),
    LUBLIN("Lublin"),
    TORUN("Toruń"),
    SZCZECIN("Szczecin");

    private final String name;

    public String getName() {
        return name;
    }

    CityEnum(String name) {
        this.name = name;
    }
}
