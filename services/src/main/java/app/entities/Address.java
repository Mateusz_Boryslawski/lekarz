package app.entities;


import app.entities.Enums.CityEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;


@Data
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String cityName;
    @Column
    private String street;
    @Column
    private String houseNumber;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "address_id")
    @JsonIgnore
    @OneToOne(mappedBy = "address", cascade = CascadeType.ALL,
        fetch = FetchType.EAGER, optional = false)
    private Doctor doctor;

    public Address() { }

    public Address(CityEnum cityEnum) {
        this.cityName = cityEnum.name();
    }

//    public void addDoctor(Doctor doctor) {
//        if(!this.doctors.contains(doctor))
//            this.doctors.add(doctor);
//    }
//
//    public void removeDoctor(Doctor doctor) {
//        if(this.doctors.contains(doctor))
//            this.doctors.remove(doctor);
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(cityName, address.cityName) &&
                Objects.equals(street, address.street) &&
                Objects.equals(houseNumber, address.houseNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cityName, street, houseNumber);
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", cityName='" + cityName + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                '}';
    }
}
