package app.entities;


import app.entities.Enums.SpecializationEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;


@Data
@Entity
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String name;
    @Column
    private String lastName;
    @Email
    @Column
    private String email;
    @Column
    private String telephone;
    @Column
    private String password;
    @Column
    private String token;
    @Column
    String specialization;
    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "doctor",
            cascade = CascadeType.ALL)
    private Collection<Opinion> opinions = new ArrayList<>();
    @Column
    private Integer mark;


    @OneToOne(fetch = FetchType.EAGER,
            cascade=CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;



    public Doctor() {
    }

    public Doctor(String name) {
        this.name = name;
    }


    public void addOpinion(Opinion opinion){
        opinion.setDoctor(this);
        this.opinions.add(opinion);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(id, doctor.id) &&
                Objects.equals(name, doctor.name) &&
                Objects.equals(lastName, doctor.lastName) &&
                Objects.equals(email, doctor.email) &&
                Objects.equals(password, doctor.password) &&
                Objects.equals(specialization, doctor.specialization) &&
                Objects.equals(address, doctor.address);

    }


    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, email, password, specialization, address);
    }


    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                ", specialization='" + specialization + '\'' +
                '}';
    }
}
