package app.filters;

import app.dto.Token;
import app.servicesImpl.CustomerContext;
import app.servicesImpl.CustomerServiceImpl;
import app.servicesImpl.DoctorContext;
import app.servicesImpl.DoctorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomerTokenFilter implements Filter {
    @Autowired
    private CustomerServiceImpl customerService;
    @Autowired
    private CustomerContext customerContext;
    @Autowired
    private DoctorServiceImpl doctorService;
    @Autowired
    private DoctorContext doctorContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if (!httpServletRequest.getServletPath().equals("/user/login") &&
                !httpServletRequest.getServletPath().equals("/user/signUp") &&
                !httpServletRequest.getServletPath().equals("/doctor/getSpecificDoctors") &&
                !httpServletRequest.getServletPath().equals("/doctor/getAllDoctors") &&
                !httpServletRequest.getServletPath().equals("/test/test") &&
                !httpServletRequest.getServletPath().equals("/test/hash") &&
                !httpServletRequest.getServletPath().equals("/test/mail") &&
                !httpServletRequest.getServletPath().equals("/test/activate") &&
                !httpServletRequest.getServletPath().equals("/doctor/login") &&
                !httpServletRequest.getServletPath().equals("/doctor/signUp") &&
                !httpServletRequest.getServletPath().equals("/doctor/getSpecificDoctors") &&
                !httpServletRequest.getServletPath().equals("/doctor/getAllDoctors") &&
                !httpServletRequest.getServletPath().equals("/test/test") &&
                !httpServletRequest.getServletPath().equals("/test/mail") &&
                !httpServletRequest.getServletPath().equals("/test/activate")
                && !"OPTIONS".equals(httpServletRequest.getMethod())) {

            if(httpServletRequest.getServletPath().contains("/doctor")){
                String token = httpServletRequest.getHeader("Authorization");
                if (token == null) { // token jest nieprawidlowy
                    httpServletResponse.sendError(401, "Zły token");
                } else {
                    Token token1 = new Token(token);
                    if (doctorService.checkDoctorToken(token1) == null) {
                        httpServletResponse.sendError(401, "Nie znaleziono uzytkownika dla tokenu");
                    } else {
                        doctorContext.setDoctor(doctorService.checkDoctorToken(token1));
                        filterChain.doFilter(servletRequest, servletResponse);

                    }
                }
            }

            if(httpServletRequest.getServletPath().contains("/user")){
                String token = httpServletRequest.getHeader("Authorization");
                if (token == null) { // token jest nieprawidlowy
                    httpServletResponse.sendError(401, "Zły token");
                } else {
                    Token token1 = new Token(token);
                    if (customerService.checkCustomerToken(token1) == null) {
                        httpServletResponse.sendError(401, "Nie znaleziono uzytkownika dla tokenu");
                    } else {
                        customerContext.setCustomer(customerService.checkCustomerToken(token1));
                        filterChain.doFilter(servletRequest, servletResponse);
                    }
                }
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
            customerContext.setCustomer(null);
        }
    }

    @Override
    public void destroy() {

    }
}