package app;

import app.entities.Doctor;
import app.entities.Enums.SpecializationEnum;
import app.servicesImpl.DoctorServiceImpl;
import app.servicesInterfaces.DoctorService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.print.Doc;
import java.util.List;


@SpringBootApplication
public class Application {


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);


//        DoctorService doctorService = new DoctorServiceImpl();
//
//        List<Doctor> list = doctorService.findAll();
//
//        Doctor doctor = new Doctor("Jan");
//        doctor.setSpecialization(SpecializationEnum.CHIRURG);
//        doctorService.save(doctor);
//
//
//        System.out.println();
    }
}
