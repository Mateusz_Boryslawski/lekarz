package app.dto;

import lombok.Data;

@Data
public class CustomerDto {
    private Long id;
    private String email;
    private String name;
    private String lastName;

    public CustomerDto() {
    }


}
