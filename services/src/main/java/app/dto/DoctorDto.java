package app.dto;


import lombok.Data;

@Data
public class DoctorDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String telephone;
    private String specialization;
    private String city;
    private String street;
}
