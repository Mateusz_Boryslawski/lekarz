package app.dto;

import app.entities.Enums.CityEnum;
import app.entities.Enums.SpecializationEnum;
import lombok.Data;

@Data
public class SpecificDoctorDto {

        SpecializationEnum specializationEnum;
        CityEnum cityEnum;

    public SpecificDoctorDto(SpecializationEnum specializationEnum, CityEnum cityEnum) {
        this.specializationEnum = specializationEnum;
        this.cityEnum = cityEnum;
    }
}
