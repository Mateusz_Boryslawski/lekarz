package app.dto;

import app.entities.Enums.CityEnum;
import app.entities.Enums.SpecializationEnum;

public class EnumsDto {
    private String cityEnum;
    private String specializationEnum;


    public String getCityEnum() {
        return cityEnum;
    }

    public void setCityEnum(String cityEnum) {
        this.cityEnum = cityEnum;
    }

    public String getSpecializationEnum() {
        return specializationEnum;
    }

    public void setSpecializationEnum(String specializationEnum) {
        this.specializationEnum = specializationEnum;
    }
}
