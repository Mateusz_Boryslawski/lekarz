package app.dto;

import app.entities.Doctor;
import lombok.Data;


@Data
public class OpinionDto {
    private Doctor doctor;
    float mark;
    String opinion;
}
