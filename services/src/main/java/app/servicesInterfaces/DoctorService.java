package app.servicesInterfaces;

import app.dto.Token;
import app.entities.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public interface DoctorService {

    Doctor save(Doctor doctor);
    Doctor update(Doctor doctor);
    void delete(Doctor doctor);
    void deleteById(Long id);
    Long count();


    Optional<Doctor> getById(Long id);
    Doctor findByName(String name);

    Page<Doctor> findAllByName(String name, Pageable p);
    Page<Doctor> findAllByNameContaining(String name, Pageable p);


    List<Doctor> findAll();

    List<Doctor> findAllByAddress_CityNameAndSpecialization(String cityName, String specialization);

    Optional<Doctor> findByEmail(String email);


    //---
    void logout();
    Token login(Doctor doctor);
    void addNewDoctor(Doctor doctor);
    Doctor checkDoctorToken(Token token);
}
