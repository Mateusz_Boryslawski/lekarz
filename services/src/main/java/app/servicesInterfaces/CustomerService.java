package app.servicesInterfaces;

import app.dto.Token;
import app.entities.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Optional;


@Service
@Transactional
public interface CustomerService {


    Customer update(Customer customer);
    void delete(Customer customer);
    void deleteById(Long id);
    Long count();

    ArrayList<Customer> findAllByFirstName(String firstName);
    ArrayList<Customer> findAllByFirstNameContaining(String firstName);

    Page<Customer> findAllByFirstName(String firstName, Pageable p);
    Page<Customer> findAllByFirstNameContaining(String firstName, Pageable p);

    Optional<Customer> getById(Long id);

    Boolean existsByFirstName(String firstName);
    Boolean existsByEmail(String email);

    Optional<Customer> findByEmail(String email);

    // ---------

    void addNewCustomer(Customer customer) throws NoSuchAlgorithmException;
    boolean activateAccount(String activateCode);
    Token login(Customer customer) throws NoSuchAlgorithmException;
    Customer checkCustomerToken(Token token);
    void logout();

}
