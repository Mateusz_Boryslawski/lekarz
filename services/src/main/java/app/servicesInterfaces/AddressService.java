package app.servicesInterfaces;


import app.entities.Address;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public interface AddressService {

    Address checkIfAddressAlreadyExistInDB(Address address);

    Address test(Address address);


    //-----------------

    Optional<Address> getById(Long id);
    void delete(Address address);
}
