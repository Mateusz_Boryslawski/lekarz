package app.controllers;


import app.GoogleMail;
import app.dto.EnumsDto;
import app.dto.OpinionDto;
import app.entities.Address;
import app.entities.Customer;
import app.entities.Doctor;
import app.entities.Enums.CityEnum;
import app.entities.Enums.SpecializationEnum;
import app.entities.Opinion;
import app.servicesImpl.DoctorServiceImpl;
import app.servicesInterfaces.CustomerService;
import app.servicesInterfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/test")
public class TestController {

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private CustomerService customerService;

    @GetMapping(path = "/hash")
    public @ResponseBody
    ResponseEntity<List<Doctor>> hash() {
        try {


            OpinionDto opinionDto = new OpinionDto();

            Optional<Doctor> userDb = doctorService.getById(401L);
            Doctor doctor = userDb.get();
            opinionDto.setDoctor(doctor);
            opinionDto.setOpinion("opinia");


            Optional<Doctor> userDb1 = doctorService.getById(opinionDto.getDoctor().getId());
            Doctor doctor1 = userDb1.get();
            doctor1.addOpinion(new Opinion(opinionDto.getOpinion()));
            doctorService.update(doctor1);

            // customerService.login(customer1);

            System.out.println("");


            return new ResponseEntity<List<Doctor>>(doctorService.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.toString());
            return new ResponseEntity<List<Doctor>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/mail")
    public @ResponseBody
    ResponseEntity<List<Doctor>> mail() {
        try {

            Customer customer = new Customer();
            String mail = "mboryslawski@gmail.com";
            customer.setEmail(mail);
            customer.setPassword("xx");

            customerService.addNewCustomer(customer);

            Optional<Customer> customer1 = customerService.findByEmail(mail);

            GoogleMail.Send(customer1.get().getEmail(), customer1.get().getActivateCode());

            System.out.println("");
            return new ResponseEntity<List<Doctor>>(doctorService.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.toString());
            return new ResponseEntity<List<Doctor>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/activate")
    public @ResponseBody
    ResponseEntity<List<Doctor>> activate() {
        try {
            Customer customer = new Customer("customer");
            customer.setEmail("mboryslawski@wp.pl");

            customerService.activateAccount("51633864");


            System.out.println("");
            return new ResponseEntity<List<Doctor>>(doctorService.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.toString());
            return new ResponseEntity<List<Doctor>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
