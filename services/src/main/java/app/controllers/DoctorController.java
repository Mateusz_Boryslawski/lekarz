package app.controllers;


import app.GoogleMail;
import app.dto.EnumsDto;
import app.dto.OpinionDto;
import app.dto.Token;
import app.entities.Address;
import app.entities.Doctor;
import app.entities.Enums.CityEnum;
import app.entities.Enums.SpecializationEnum;
import app.entities.Opinion;
import app.servicesImpl.DoctorServiceImpl;
import app.servicesInterfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/doctor")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;


    @GetMapping(path = "/getAllDoctors")
    public @ResponseBody
    ResponseEntity<List<Doctor>> getAllDoctorsController() {

        try {
            return new ResponseEntity<List<Doctor>>(doctorService.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Doctor>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/getSpecificDoctors")
    public @ResponseBody
    ResponseEntity<List<Doctor>> doctorController(@RequestBody EnumsDto enumsDto) {
        try {
            return new ResponseEntity<List<Doctor>>(doctorService.
                    findAllByAddress_CityNameAndSpecialization(enumsDto.getCityEnum(), enumsDto.getSpecializationEnum()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Doctor>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/signUp")
    public @ResponseBody
    ResponseEntity<Void> addNewDoctorController(@RequestBody Doctor doctor) {
        try {
            doctorService.addNewDoctor(doctor);

//            Optional<Doctor> userDB= doctorService.findByEmail(doctor.getEmail());
//            Doctor doctor1 = userDB.get();

            System.out.println("");

            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/login")
    public @ResponseBody
    ResponseEntity<Token> loginController(@RequestBody Doctor doctor) {
        try {
            return new ResponseEntity<Token>(doctorService.login(doctor), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Token>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/logout")
    public @ResponseBody
    ResponseEntity<Void> logoutController() {
        try {
            doctorService.logout();
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
