package app.controllers;

import app.GoogleMail;
import app.dto.OpinionDto;
import app.dto.Token;
import app.entities.Customer;
import app.entities.Doctor;
import app.entities.Opinion;
import app.servicesInterfaces.CustomerService;
import app.servicesInterfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private DoctorService doctorService;


    @PostMapping(path = "/signUp")
    public @ResponseBody
    ResponseEntity<Void> addNewUserController(@RequestBody Customer customer) {
        try {
            customerService.addNewCustomer(customer);

            Optional<Customer> userDB= customerService.findByEmail(customer.getEmail());
            Customer customer1 = userDB.get();
            GoogleMail.Send(customer1.getEmail(), customer1.getActivateCode());

            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/activateUser")
    public @ResponseBody
    ResponseEntity<Void> ActivateUserController(@RequestParam String code) {
        try {
                boolean value = customerService.activateAccount(code);

            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/login")
    public @ResponseBody
    ResponseEntity<Token> loginController(@RequestBody Customer customer) {
        try {
            return new ResponseEntity<Token>(customerService.login(customer), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Token>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/logout")
    public @ResponseBody
    ResponseEntity<Void> logoutController() {
        try {
            customerService.logout();
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/addOpinion")
    public @ResponseBody
    ResponseEntity<Void> addOpinion(@RequestBody OpinionDto opinionDto) {
        try {

            Optional<Doctor> userDb = doctorService.getById(opinionDto.getDoctor().getId());
            Doctor doctor = userDb.get();
            doctor.addOpinion(new Opinion(opinionDto.getOpinion()));
            doctorService.update(doctor);

            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
