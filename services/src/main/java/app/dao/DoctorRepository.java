package app.dao;

import app.dto.Token;
import app.entities.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Transactional
public interface DoctorRepository
    extends JpaRepository<Doctor, Long> {

    Doctor findByName(String name);

    Optional<Doctor> getById(Long id);

    Page<Doctor> findAllByName(String name, Pageable p);
    Page<Doctor> findAllByNameContaining(String name, Pageable p);
    Page<Doctor> findAllByLastNameContaining(String lastName, Pageable p);

    List<Doctor> findAll();

    // List<Doctor> findAllByAddress_CityNameAndSpecializations(String cityName, String specialization);

    List<Doctor> findAllByAddress_CityNameAndSpecialization(String cityName, String specialization);

    List<Doctor> findAllByAddress_CityName(String cityName);


    Optional<Doctor> findByToken(String token);
    Optional<Doctor> findByEmail(String email);




}
