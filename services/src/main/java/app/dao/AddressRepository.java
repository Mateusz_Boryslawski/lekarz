package app.dao;


import app.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface AddressRepository
        extends JpaRepository<Address, Long> {

    Optional<Address> getByCityNameAndStreetAndHouseNumber(
            String cityName, String street, String houseNumber);

    Optional<Address> getAddressByCityName(String cityName);

    //--------------

    Optional<Address> getById(Long id);
    Collection<Address> findAddressById(Long id);
}
