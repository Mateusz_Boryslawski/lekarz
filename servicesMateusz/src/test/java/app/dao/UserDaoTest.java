package app.dao;

import app.Application;
import app.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
class UserDaoTest {
    @Autowired
    private UserDao userDao;

    @Test
    void dodajUzytkownika() {
        User user = new User();
        user.setName("Michal");
        user.setEmail("michal.chudy21@gmail.com");
        user.setLastName("chudy");
        userDao.save(user);
    }
}