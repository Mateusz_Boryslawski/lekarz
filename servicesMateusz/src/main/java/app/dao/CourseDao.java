package app.dao;

import app.dto.QuestionDto;
import app.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface CourseDao extends JpaRepository<Course, Integer> {
    <S extends Course> S save(S entity);
    void deleteById(Integer id);
    void delete(Course entity);
    Optional<Course> findById(Integer id);
    List<Course> findAll();
    List<Course> findByUserId(Integer userId);
    //long count();
    //void delete(Course entity);
    //boolean exists(Integer id);
}
