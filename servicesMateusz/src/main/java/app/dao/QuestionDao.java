package app.dao;

import app.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Repository
@Transactional
public interface QuestionDao extends JpaRepository<Question, Integer> {
    <S extends Question> S save(S question);
    Optional<Question> findById(Integer id);
    void deleteById(Integer id);
}
