package app.dao;

import app.entities.Testmodule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface TestmoduleDao extends JpaRepository<Testmodule, Integer> {
    <S extends Testmodule> S save(S instance);

    List<Testmodule> findAll();

    void delete(Testmodule instance);

    Optional<Testmodule> findById(Integer id);
}
