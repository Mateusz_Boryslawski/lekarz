package app.dao;

import app.entities.TestQuestion;
import app.entities.Testmodule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface TestQuestionDao extends JpaRepository<TestQuestion, Integer> {
    <S extends TestQuestion> S save(S instance);

    List<TestQuestion> findAll();

    void delete(TestQuestion instance);

    Optional<TestQuestion> findById(Integer id);
}