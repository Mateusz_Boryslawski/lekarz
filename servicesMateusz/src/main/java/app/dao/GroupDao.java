package app.dao;

import app.entities.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface GroupDao extends JpaRepository<Group, Integer> {
    <S extends Group> S save(S group);
    Optional<Group> findById(Integer id);
    void deleteById(Integer id);
    void delete(Group group);
    List<Group> findAll();
}
