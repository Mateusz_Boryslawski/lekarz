package app.dao;


import app.entities.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ResultDao extends JpaRepository<Result, Integer> {
    <S extends Result> S save(S result);

    List<Result> findAll();

    void delete(Result result);
}
