package app.dao;


import app.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface UserDao extends JpaRepository<User, Integer> {
    <S extends User> S save(S user);
    Optional<User> findById(Integer id);
    Optional<User> findByEmail(String email);
    Optional<User> findByToken(String token);
}
