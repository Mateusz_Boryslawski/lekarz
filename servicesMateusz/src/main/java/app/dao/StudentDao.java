package app.dao;

import app.entities.Student;
import app.entities.Testmodule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface StudentDao extends JpaRepository<Student, Integer> {
    <S extends Student> S save(S student);

    List<Student> findAll();

    Optional<Student> findByIndexNumber(Integer indexNumber);

    void delete(Student student);
}
