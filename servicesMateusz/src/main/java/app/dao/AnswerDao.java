package app.dao;

import app.entities.Answer;
import app.entities.Question;
import app.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface AnswerDao extends JpaRepository<Answer, Integer> {
    <S extends Answer> S save(S question);
    Optional<Answer> findById(Integer id);
    void deleteById(Integer id);
    void deleteInBatchByQuestionId(Integer questionId);
}
