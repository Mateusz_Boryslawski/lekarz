package app;

import app.entities.Course;
import app.entities.Group;
import app.entities.Result;
import app.entities.Student;

import java.util.ArrayList;
import java.util.List;

public class CsvConverter {
    private char _separator = ';';

    public List<String> GetMarks(Student student) {
        List<String> list = GetAll(student);
        list.remove(2);
        return list;
    }

    public List<String> GetMarks(Group group) {
        List<String> list = new ArrayList<>();
        for (Student student : group.getStudents()) {
            list.addAll(GetMarks(student));
        }
        return list;
    }

    public List<String> GetMarks(Course course) {
        List<String> list = new ArrayList<>();
        for (Group group : course.getGroups()) {
            list.addAll(GetMarks(group));
        }
        return list;
    }

    public List<String> GetPercentageResults(Student student) {
        List<String> list = GetAll(student);
        list.remove(1);
        return list;
    }

    public List<String> GetPercentageResults(Group group) {
        List<String> list = new ArrayList<>();
        for (Student student : group.getStudents()) {
            list.addAll(GetPercentageResults(student));
        }
        return list;
    }

    public List<String> GetPercentageResults(Course course) {
        List<String> list = new ArrayList<>();
        for (Group group : course.getGroups()) {
            list.addAll(GetPercentageResults(group));
        }
        return list;
    }

    private List<String> GetAll(Student student) {
        StringBuilder columnNames = new StringBuilder();
        columnNames.append("First Name" + _separator + "Last Name" + _separator);

        StringBuilder marks = new StringBuilder();
        marks.append(student.getName() + _separator + student.getLastName() + _separator);

        StringBuilder percentageResults = new StringBuilder();
        percentageResults.append(student.getName() + _separator + student.getLastName() + _separator);

        for (Result result : student.getResults()) {
            columnNames.append(result.getTest().getName() + _separator);
            marks.append(result.getMark() + _separator);
            percentageResults.append(result.getPercentageResult() + _separator);
        }
        List<String> list = new ArrayList<String>();
        list.add(columnNames.toString());
        list.add(marks.toString());
        list.add(percentageResults.toString());
        return list;
    }
}
