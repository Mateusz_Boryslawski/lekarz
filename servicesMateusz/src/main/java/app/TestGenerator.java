package app;

import app.entities.TestAnswer;
import app.entities.TestQuestion;
import app.entities.Testmodule;
import net.sf.jasperreports.engine.*;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class TestGenerator {

    public byte[] compileReportToPdf(String title, Testmodule testmodule, String pdfOutputFilename) {
        try {
            List<TestQuestion> listOfQuestionsType = new ArrayList(testmodule.getQuestions());

            TestQuestionPreprocessor preprocessor = new TestQuestionPreprocessor();
            List<TestQuestion> preprocessedQuestions = preprocessor.preprocess(listOfQuestionsType);


            List<String> listOfQuestions = new ArrayList<>();
            for (TestQuestion elem : preprocessedQuestions)
                listOfQuestions.add(elem.toString());
            //Compiling Subreport
            String subreportPath = getPathToResource("test_subreport.jrxml");
            String compiledSubreportPath = getPathToResource("subreport.jasper");
            compileReportToFile(subreportPath, compiledSubreportPath);


            // Compile final raport
            String mainReportPath = getPathToResource("test_template.jrxml");
            String compiledMainReportPath = getPathToResource("report.jasper");
            compileReportToFile(mainReportPath, compiledMainReportPath);

            //Generate Id String
            String idString = "TEST ID: " + testmodule.getId().toString();

            // Parameters for report
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("title", title);
            parameters.put("id", idString);
            parameters.put("listParam", listOfQuestions);
            parameters.put("SUBREPORT_DIR", compiledSubreportPath);

            // DataSource
            // No database connected to report
            JRDataSource dataSource = new JREmptyDataSource();

            //Linking data
            JasperPrint jasperPrint = JasperFillManager.fillReport(compiledMainReportPath,
                    parameters, dataSource);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, bos);
            byte[] bytesData = bos.toByteArray();
            // Export to PDF.
            JasperExportManager.exportReportToPdfFile(jasperPrint,
                    pdfOutputFilename);
            System.out.println("Done!");
            return bytesData;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    private String getPathToResource(String filename) throws Exception {
        String current = new java.io.File(".").getCanonicalPath();
        String resourcePath = current.concat("/resources").concat("/").concat(filename);
        return resourcePath;
    }

    private void compileReportToFile(String inputFilename, String outputFilename) throws Exception {
        File f = new File(outputFilename);
        if (!f.exists()) {
            JasperCompileManager.compileReportToFile(inputFilename, outputFilename);
        } else {
            System.out.println("Skipping compilation: ".concat(outputFilename).concat(" exit!"));
        }
    }

    private class TestQuestionPreprocessor {
        public List<TestQuestion> preprocess(List<TestQuestion> questions) {
            List<TestQuestion> result = new ArrayList<>();
            int numberOfQuestions = questions.size();
            for (int i = 0; i < numberOfQuestions; ++i) {
                result.add(preprocessQuestion(questions.get(i), i + 1));
            }
            return result;
        }

        private TestQuestion preprocessQuestion(TestQuestion question, Integer numberOfQuestion) {
            String questionText = question.getText();
            List<TestAnswer> answers = question.getAnswers();

            String preprocessedQuestion = numberOfQuestion.toString().concat(". ").concat(questionText);
            List<TestAnswer> preprocessedAnswers = preprocessAnswers(answers);

            question.setText(preprocessedQuestion);
            question.setAnswers(preprocessedAnswers);

            return question;
        }

        private List<TestAnswer> preprocessAnswers(List<TestAnswer> answers) {
            List<TestAnswer> result = new ArrayList<>();
            int numberOfAnswers = answers.size();
            for (int i = 0; i < numberOfAnswers; ++i) {
                result.add(preprocessAnswer(answers.get(i), i));
            }
            return result;
        }

        private TestAnswer preprocessAnswer(TestAnswer answer, int number) {
            String answerText = answer.getText();

            String gap = "     [   ]         ";
            String result = gap.concat(getCharForNumber(number)).concat(". ").concat(answerText);


            TestAnswer resultAnswer = new TestAnswer();
            resultAnswer.setText(result);
            return resultAnswer;
        }

        private String getCharForNumber(int i) {
            return i >= 0 && i < 26 ? String.valueOf((char) (i + 97)) : null;
        }
    }
}
