package app.controllers;

import app.QuestionSelector;
import app.TestGenerator;
import app.dto.FilledTestDto;
import app.dto.QuestionDto;
import app.dto.TestPropertiesDto;
import app.entities.*;
import app.exception.NotFoundException;
import app.exception.PermissionDeniedException;
import app.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/test")
public class StudentTestController {

    @Autowired
    StudentTestService studentTestService;

    @Autowired
    StudentService studentService;

    @Autowired
    CourseService courseService;

    @Autowired
    TestmoduleService testmoduleService;

    @Autowired
    TestGenerator testGenerator;

    @Autowired
    ResultService resultService;

    @PostMapping(path = "/question/add")
    public ResponseEntity<Void> addQuestionController(@RequestBody QuestionDto questionDto) {
        try {
            studentTestService.addNewQuestion(questionDto);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/question/delete")
    public ResponseEntity<Void> deleteQuestionController(@RequestParam Integer questionId) {
        try {
            studentTestService.deleteQuestion(questionId);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(NotFoundException e){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        catch (PermissionDeniedException e) {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/question/update")
    public ResponseEntity<Void> updateQuestionController(@RequestBody QuestionDto questionDto) {
        try {
            studentTestService.updateQuestion(questionDto);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch(NotFoundException e){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        catch (PermissionDeniedException e) {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/generate")
    public ResponseEntity<byte[]> addTest(@RequestBody TestPropertiesDto testPropertiesDto) {
        System.out.println("GENERATING TESTS FOR COURSE: ");
        System.out.println(testPropertiesDto.getCourseId());

        Optional<Course> course = courseService.getCourse(testPropertiesDto.getCourseId());
        if (!course.isPresent()) {
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
        }

        System.out.println("Course found!");

        Testmodule testmodule = new Testmodule();
        testmodule.setName(testPropertiesDto.getTestname());
        testmodule.setUsesNegativePoints(testPropertiesDto.getAreNegativePointsEnabled());
        testmodule.setThresholdFor3(testPropertiesDto.getThresholdFor3());
        testmodule.setThresholdFor4(testPropertiesDto.getThresholdFor4());
        testmodule.setThresholdFor5(testPropertiesDto.getThresholdFor5());
        testmoduleService.add(testmodule);

        QuestionSelector questionSelector = new QuestionSelector(course.get());
        questionSelector.getQuestions(  testmodule,
                                        testPropertiesDto.getNumberOfQuestions(),
                                        testPropertiesDto.getNumberOfAnswers(),
                                        testPropertiesDto.getHasMultianswerQuestions());
        testmoduleService.add(testmodule);

        byte []pdfData = testGenerator.compileReportToPdf(testPropertiesDto.getTestname(), testmodule, testPropertiesDto.getPdfOutputFilename());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "output.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdfData, headers, HttpStatus.OK);
        return response;
    }

    @PostMapping(path = "/filledTest")
    public ResponseEntity<Void> updateQuestionController(@RequestBody FilledTestDto filledTestDto) {
        Optional<Student> student = studentService.findByIndexNumber(filledTestDto.getStudentIndexNumber());
        if (!student.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        Optional<Testmodule> testmodule = testmoduleService.get(filledTestDto.getTestmoduleId());
        if (!testmodule.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        Result result = checkAnswers(testmodule.get(), filledTestDto);

        // Saving
        result.setStudent(student.get());
        result.setTest(testmodule.get());
        student.get().getResults().add(result);
        studentService.add(student.get());
        testmodule.get().getResults().add(result);
        //testmoduleService.add(testmodule.get());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    private Result checkAnswers(Testmodule testmodule, FilledTestDto filledTestDto)
    {
        Integer points = 0;

        for (int i = 0; i < testmodule.getQuestions().size(); ++i)
        {
            if (areMatching(testmodule.getQuestions().get(i), filledTestDto.getAnswers().get(i)))
            {
                points++;
            }
            else if (testmodule.getUsesNegativePoints())
            {
                points--;
            }
        }

        Result result = new Result();
        Double percentageResult = points * 1.0 / testmodule.getQuestions().size();
        result.setMark(calculateMark(testmodule, percentageResult));
        result.setPercentageResult(percentageResult);
        return result;
    }

    private String calculateMark(Testmodule testmodule, Double percentageResult)
    {
        if (percentageResult < testmodule.getThresholdFor3())
            return "2";
        if (percentageResult < testmodule.getThresholdFor4())
            return "3";
        if (percentageResult < testmodule.getThresholdFor5())
            return "4";
        return "5";
    }

    private Boolean areMatching(TestQuestion question, List<Integer> answers)
    {
        Integer numberOfCorrectAnswers = 0;
        for (TestAnswer answer : question.getAnswers()) {
            if (answer.getIsCorrect())
                numberOfCorrectAnswers++;
        }

        if (numberOfCorrectAnswers != answers.size())
            return false;

        for (Integer answer : answers) {
            if (!question.getAnswers().get(answer).getIsCorrect())
                return false;
        }
        return true;
    }
}
