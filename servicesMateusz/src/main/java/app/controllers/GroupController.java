package app.controllers;

import app.CsvConverter;
import app.dto.GroupDto;
import app.dto.StudentDto;
import app.entities.Course;
import app.entities.Group;
import app.entities.Student;
import app.services.CourseService;
import app.services.GroupService;
import app.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private CourseService courseService;

    @PostMapping(path = "/add")
    public ResponseEntity<Void> addGroupController(@RequestBody GroupDto groupDto) {
        try {
            Group group = new Group();
            group.setGroupName(groupDto.getGroupName());
            Optional<Course> course = courseService.getCourse(groupDto.getCourse().getId());
            if (!course.isPresent()) {
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
            group.setCourse(course.get());
            groupService.add(group);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/delete")
    public ResponseEntity<Void> deleteGroupController(@RequestBody GroupDto groupDto) {
        try {
            Optional<Group> group = groupService.findById(groupDto.getId());
            if (!group.isPresent()) {
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
            if (group.get().getStudents().size() != 0) {
                return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
            }
            groupService.remove(group.get());
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/addStudent")
    public ResponseEntity<Void> addStudentController(@RequestBody StudentDto studentDto) {
        try {
            Student student = new Student();
            student.setName(studentDto.getName());
            student.setLastName(studentDto.getLastName());
            student.setIndexNumber(studentDto.getIndexNumber());
            Optional<Group> group = groupService.findById(studentDto.getGroup().getId());
            if (!group.isPresent()) {
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
            student.setGroup(group.get());
            studentService.add(student);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/deleteStudent")
    public ResponseEntity<Void> deleteStudentController(@RequestBody StudentDto studentDto) {
        try {
            Optional<Student> student = studentService.findByIndexNumber(studentDto.getIndexNumber());
            if (!student.isPresent()) {
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
            studentService.remove(student.get());
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getByCourse")
    public @ResponseBody
    ResponseEntity<List<GroupDto>> getAllCoursesController(@RequestParam("courseId") Integer courseId) {
        try {
            Optional<Course> course = courseService.getCourse(courseId);
            if (!course.isPresent()) {
                return new ResponseEntity<List<GroupDto>>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<List<GroupDto>>(course.get().getGroups().stream().map( g -> g.getDto() ).collect( Collectors.toList() ), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<List<GroupDto>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
