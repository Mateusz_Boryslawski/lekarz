package app.controllers;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CorsFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        response.addHeader("Access-Control-Allow-Origin", "*");

        if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
            // CORS "pre-flight" request
            response.addHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
            response.addHeader("Access-Control-Allow-Headers", "X-Requested-With,Origin, Content-Type,Accept," +
                    "Access-Control-Request-Headers,Cache-Control, Authorization");
        } else {
            response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
            response.addHeader("Pragma", "no-cache"); // HTTP 1.0
            response.addHeader("Expires", "0"); // Proxies
        }

        filterChain.doFilter(request, response);
    }
}