package app.controllers;

import app.CsvConverter;
import app.dto.AnswerDto;
import app.dto.CourseDto;
import app.dto.QuestionDto;
import app.dto.TestPropertiesDto;
import app.entities.Answer;
import app.entities.Course;
import app.entities.Question;
import app.exception.NotFoundException;
import app.exception.PermissionDeniedException;
import app.services.AnswerService;
import app.services.CourseService;
import app.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/course")
public class CourseGroupController {

    @Autowired
    CourseService courseService;

    @Autowired
    QuestionService questionService;

    @Autowired
    AnswerService answerService;

    @PostMapping(path = "/addAnswer")
    public @ResponseBody
    ResponseEntity<Void> addAnswerController(@RequestBody AnswerDto answerDto) {
        try {
            Optional<Question> question = questionService.findById(answerDto.getQuestion().getId());
            if (!question.isPresent()) {
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
            }
            Answer answer = new Answer();
            answer.setText(answerDto.getText());
            answer.setIsCorrect(answerDto.isCorrect());
            answer.setQuestion(question.get());
            question.get().getAnswers().add(answer);
            questionService.add(question.get());
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getAnswers")
    public @ResponseBody
    ResponseEntity<QuestionDto> getAnswersController(@RequestParam("questionId") Integer questionId) {
        try {
            Optional<Question> question = questionService.findById(questionId);
            if (!question.isPresent()) {
                return new ResponseEntity<QuestionDto>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<QuestionDto>(question.get().getDto(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<QuestionDto>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/deleteAnswer")
    public @ResponseBody
    ResponseEntity<Void> deleteAnswerController(@RequestBody AnswerDto answerDto) {
        try {
            Optional<Answer> answer = answerService.findById(answerDto.getId());
            answerService.remove(answer.get());
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/editAnswer")
    public @ResponseBody
    ResponseEntity<Void> editAnswerController(@RequestBody AnswerDto answerDto) {
        try {
            answerService.updateAnswer(answerDto);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/addCourse")
    public @ResponseBody
    ResponseEntity<Void> addCourseController(@RequestBody CourseDto courseDto) {
        try {
            courseService.addCourse(new Course(courseDto));
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getAllCourses")
    public @ResponseBody
    ResponseEntity<List<CourseDto>> getAllCoursesController() {
        try {
            return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getUserCourses")
    public @ResponseBody
    ResponseEntity<List<CourseDto>> getUserCoursesController() {
        try {
            return new ResponseEntity<>(courseService.getUserCourses(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getCourse")
    public @ResponseBody
    ResponseEntity<CourseDto> getCourseByIdController(@RequestParam("courseId") Integer courseId) {
        try {
            Optional<Course> course = courseService.getCourse(courseId);
            if (!course.isPresent()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(course.get().getDto(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/deleteCourse")
    public @ResponseBody
    ResponseEntity<Void> deleteCourseByIdController(@RequestParam("courseId") Integer courseId) {
        try {
            courseService.deleteCourse(courseId);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/updateCourse")
    public @ResponseBody
    ResponseEntity<Void> updateCourseByIdController(@RequestBody CourseDto courseDto) {
        try {
            courseService.updateCourse(courseDto);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } catch (PermissionDeniedException e) {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/test")
    public ResponseEntity<Void> testCourse() {
        Optional<Course> course = courseService.getCourse(2);
        if (!course.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        Collection<Question> questions = course.get().getQuestions();
        for (Question q : questions) {
            System.out.println(q.toString());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping(path = "/csv")
    public ResponseEntity<byte[]> addTest(@RequestParam("courseId") Integer courseId) {
        System.out.println("GENERATING CSV FOR Course: " + courseId);
        Optional<Course> course = courseService.getCourse(courseId);
        if (!course.isPresent()) {
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
        }
        CsvConverter csvConverter = new CsvConverter();
        List<String> csvlist = csvConverter.GetMarks(course.get());
        String csvString = "";
        for (String s : csvlist) {
            csvString.concat(s + '\n');
        }
        byte []csvData = csvString.getBytes();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("text/csv"));
        String filename = "output.csv";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(csvData, headers, HttpStatus.OK);
        return response;
    }

}