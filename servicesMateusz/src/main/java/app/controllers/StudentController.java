package app.controllers;

import app.CsvConverter;
import app.dto.StudentDto;
import app.entities.Group;
import app.entities.Student;
import app.services.GroupService;
import app.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private GroupService groupService;

    @PostMapping(path = "/add")
    public ResponseEntity<Void> addQuestionController(@RequestBody StudentDto studentDto) {
        try {
            Student student = new Student();
            student.setName(studentDto.getName());
            student.setLastName(studentDto.getLastName());
            student.setIndexNumber(studentDto.getIndexNumber());
            Optional<Group> group = groupService.findById(studentDto.getGroup().getId());
            if (!group.isPresent()) {
                return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            student.setGroup(group.get());
            studentService.add(student);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getAllStudents")
    public @ResponseBody
    ResponseEntity<List<StudentDto>> getAllCoursesController() {
        try {
            return new ResponseEntity<List<StudentDto>>(studentService.getAll().stream().map(s -> s.getDto()).collect(Collectors.toList()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<StudentDto>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/getStudentsByGroup")
    public @ResponseBody
    ResponseEntity<List<StudentDto>> getByGroupController(@RequestParam("groupId") Integer groupId) {
        try {
            Optional<Group> group = groupService.findById(groupId);
            if (!group.isPresent()) {
                return new ResponseEntity<List<StudentDto>>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<List<StudentDto>>(group.get().getStudents().stream().map(g -> g.getDto()).collect(Collectors.toList()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<StudentDto>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/csv")
    public ResponseEntity<byte[]> addTest(@RequestBody StudentDto studentDto) {
        System.out.println("GENERATING CSV FOR STUDENT: " + studentDto.getName() + " " + studentDto.getLastName());
        Optional<Student> student = studentService.findByIndexNumber(studentDto.getIndexNumber());
        if (!student.isPresent()) {
            return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
        }
        CsvConverter csvConverter = new CsvConverter();
        List<String> csvlist = csvConverter.GetMarks(student.get());
        String csvString = "";
        for (String s : csvlist) {
            csvString = csvString.concat(s + '\n');
        }
        byte[] csvData = csvString.getBytes();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("text/csv"));
        String filename = "output.csv";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(csvData, headers, HttpStatus.OK);
        return response;
    }
}
