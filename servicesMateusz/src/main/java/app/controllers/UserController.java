package app.controllers;

import app.dto.Token;
import app.entities.User;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(path = "/signUp")
    public @ResponseBody
    ResponseEntity<Void> addNewUserController(@RequestBody User user) {
        try {
            userService.addNewUser(user);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/login")
    public @ResponseBody
    ResponseEntity<Token> loginController(@RequestBody User user) {
        try {
            return new ResponseEntity<Token>(userService.login(user), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Token>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/logout")
    public @ResponseBody
    ResponseEntity<Void> logoutController() {
        try {
            userService.logout();
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
