package app.dto;

public class CourseDto {
    private Integer id;
    private Integer userId;
    private String courseName;
    private QuestionDto[] courseQuestions;
    // TODO: add groups

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public QuestionDto[] getCourseQuestions() {
        return courseQuestions;
    }

    public void setCourseQuestions(QuestionDto[] courseQuestions) {
        this.courseQuestions = courseQuestions;
    }
}
