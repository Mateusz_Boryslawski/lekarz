package app.dto;

import java.util.List;
import java.util.Map;

public class FilledTestDto {
    private Integer studentIndexNumber;
    private Integer testmoduleId;

    /*
    Sample json:
    {
        "studentIndexNumber" : 213526,
        "testmoduleId" : 34,
        "answers" : [
            [1,3,4],  // Answers 1, 3 and 4 for question 1
            [2],      // Answer 2 for question 2
            []        // No answers for question 3
        ]
    }
     */
    private List<List<Integer>> answers;

    public Integer getTestmoduleId() {
        return testmoduleId;
    }
    public void setTestmoduleId(Integer testmoduleId) {
        this.testmoduleId = testmoduleId;
    }

    public Integer getStudentIndexNumber() {
        return studentIndexNumber;
    }
    public void setStudentIndexNumber(Integer studentIndexNumber) {
        this.studentIndexNumber = studentIndexNumber;
    }

    public List<List<Integer>> getAnswers() {
        return answers;
    }
    public void setAnswers(List<List<Integer>> answers) {
        this.answers = answers;
    }
}
