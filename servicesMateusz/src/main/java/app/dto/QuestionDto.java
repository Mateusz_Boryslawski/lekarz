package app.dto;

public class QuestionDto {
    private Integer id;
    private Integer courseId;
    private String text;
    private AnswerDto[] answers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AnswerDto[] getAnswers() {
        return answers;
    }

    public void setAnswers(AnswerDto[] answers) {
        this.answers = answers;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(text);
        sb.append(": ");
        if (answers != null) {
            for (AnswerDto a : answers) {
                sb.append(a.toString());
                sb.append(", ");
            }
        }
        return sb.toString();
    }
}
