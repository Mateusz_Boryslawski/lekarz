package app.dto;

public class TestPropertiesDto {
    private Integer courseId;
    private String testname;
    private Boolean hasMultianswerQuestions;
    private Integer numberOfQuestions;
    private Integer numberOfAnswers;
    private Integer numberOfGroups;
    private Boolean areNegativePointsEnabled;
    private String pdfOutputFilename;
    private Double thresholdFor3;
    private Double thresholdFor4;
    private Double thresholdFor5;

    public Double getThresholdFor3() {
        return thresholdFor3;
    }
    public void setThresholdFor3(Double thresholdFor3) {
        this.thresholdFor3 = thresholdFor3;
    }

    public Double getThresholdFor4() {
        return thresholdFor4;
    }
    public void setThresholdFor4(Double thresholdFor4) {
        this.thresholdFor4 = thresholdFor4;
    }

    public Double getThresholdFor5() {
        return thresholdFor5;
    }
    public void setThresholdFor5(Double thresholdFor5) {
        this.thresholdFor5 = thresholdFor5;
    }

    public Integer getCourseId() {
        return courseId;
    }
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getTestname() {
        return testname;
    }
    public void setTestname(String testname) {
        this.testname = testname;
    }

    public Boolean getHasMultianswerQuestions() {
        return hasMultianswerQuestions;
    }
    public void setHasMultianswerQuestions(Boolean hasMultianswerQuestions) {
        this.hasMultianswerQuestions = hasMultianswerQuestions;
    }

    public Integer getNumberOfQuestions() {
        return numberOfQuestions;
    }
    public void setNumberOfQuestions(Integer numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public Integer getNumberOfAnswers() {
        return numberOfAnswers;
    }
    public void setNumberOfAnswers(Integer numberOfAnswers) {
        this.numberOfAnswers = numberOfAnswers;
    }

    public Integer getNumberOfGroups() {
        return numberOfGroups;
    }
    public void setNumberOfGroups(Integer numberOfGroups) {
        this.numberOfGroups = numberOfGroups;
    }

    public Boolean getAreNegativePointsEnabled() {
        return areNegativePointsEnabled;
    }
    public void setAreNegativePointsEnabled(Boolean areNegativePointsEnabled) {
        this.areNegativePointsEnabled = areNegativePointsEnabled;
    }

    public String getPdfOutputFilename() {
        return pdfOutputFilename;
    }
    public void setPdfOutputFilename(String pdfOutputFilename) {
        this.pdfOutputFilename = pdfOutputFilename;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(hasMultianswerQuestions);
        sb.append(", ");
        sb.append(numberOfQuestions);
        sb.append(", ");
        sb.append(numberOfAnswers);
        sb.append(", ");
        sb.append(numberOfGroups);
        sb.append(", ");
        sb.append(areNegativePointsEnabled);
        return sb.toString();

    }

}
