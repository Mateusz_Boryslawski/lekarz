package app.dto;

import app.entities.Course;
import app.entities.Student;
import app.entities.Testmodule;

public class GroupDto {
    private Integer id;
    private String groupName;
    private Student[] students;
    private Testmodule[] testmodules;
    private Course course;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Student[] getStudents() {
        return students;
    }
    public void setStudents(Student[] students) {
        this.students = students;
    }

    public Course getCourse() {
        return course;
    }
    public void setCourse(Course course) {
        this.course = course;
    }
}
