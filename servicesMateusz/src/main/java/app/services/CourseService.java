package app.services;

import app.dao.CourseDao;
import app.dao.UserDao;
import app.dto.CourseDto;
import app.dto.QuestionDto;
import app.entities.Course;
import app.entities.User;
import app.exception.NotFoundException;
import app.exception.PermissionDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CourseService {
    @Autowired
    CourseDao courseDao;

    @Autowired
    UserDao userDao;

    @Autowired
    UserContext userContext;


    public void addCourse(Course course) throws Exception{
        Optional<User> user = userDao.findById(userContext.getUser().getId());
        if(user.isPresent()) {
            course.setUser(user.get());
            courseDao.save(course);
        } else {
            throw new Exception("User not found");
        }

    }

    public void deleteCourse(Integer courseId) throws NotFoundException, PermissionDeniedException {
        User user = userContext.getUser();
        Optional<Course> c = courseDao.findById(courseId);
        if(!c.isPresent()) {
            throw new NotFoundException("Course not found");
        }
        Course course = c.get();
        if(!course.getUser().getId().equals(user.getId())) {
            throw new PermissionDeniedException();
        }
        courseDao.delete(course);
    }

    public void updateCourse(CourseDto courseDto) throws NotFoundException, PermissionDeniedException {
        User user = userContext.getUser();
        Optional<Course> c = courseDao.findById(courseDto.getId());
        if(!c.isPresent()) {
            throw new NotFoundException("Course not found");
        }
        Course course = c.get();
        if(!course.getUser().getId().equals(user.getId())) {
            throw new PermissionDeniedException();
        }
        course.updateFromDto(courseDto);
    }

    public List<CourseDto> getCourses() {
        List<Course> courses = courseDao.findAll();
        List<CourseDto> courseDtos = courses.stream().map( course -> course.getDto() ).collect( Collectors.toList() );
        return courseDtos;
    }

    public List<CourseDto> getUserCourses() {
        User user = userContext.getUser();
        List<Course> courses = courseDao.findByUserId(user.getId());
        List<CourseDto> courseDtos = courses.stream().map( course -> course.getDto() ).collect( Collectors.toList() );
        return courseDtos;
    }

    public Optional<Course> getCourse(Integer courseId) {
        return courseDao.findById(courseId);
    }

}
