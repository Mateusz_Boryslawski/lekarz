package app.services;

import app.dao.AnswerDao;
import app.dto.AnswerDto;
import app.entities.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AnswerService {

    @Autowired
    AnswerDao answerDao;

    public void add(Answer answer) {
        answerDao.save(answer);
    }

    public List<Answer> getAll() {
        return answerDao.findAll();
    }

    public Optional<Answer> findById(Integer id) {
        return answerDao.findById(id);
    }

    public void remove(Answer answer) {
        answerDao.delete(answer);
    }

    public void updateAnswer(AnswerDto answerDto) {
        Optional<Answer> a = answerDao.findById(answerDto.getId());
        if (a.isPresent()) {
            Answer answer = a.get();
            answer.setIsCorrect(answerDto.getIsCorrect());
            answer.setText(answerDto.getText());
            answerDao.save(answer);
        }
    }
}

