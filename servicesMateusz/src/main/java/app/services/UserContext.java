package app.services;

import app.entities.User;
import org.springframework.stereotype.Service;

@Service
public class UserContext extends ThreadLocal {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}