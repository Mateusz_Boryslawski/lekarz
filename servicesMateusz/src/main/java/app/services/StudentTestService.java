package app.services;

import app.dao.AnswerDao;
import app.dao.CourseDao;
import app.dao.QuestionDao;
import app.dto.QuestionDto;
import app.entities.Course;
import app.entities.Question;
import app.entities.User;
import app.exception.NotFoundException;
import app.exception.PermissionDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class StudentTestService {

    @Autowired
    QuestionDao questionDao;

    @Autowired
    AnswerDao answerDao;

    @Autowired
    CourseDao courseDao;

    @Autowired
    UserContext userContext;

    public void addNewQuestion(QuestionDto questionDto) throws PermissionDeniedException, NotFoundException {
        Optional<Course> c = courseDao.findById(questionDto.getCourseId());
        if (!c.isPresent()) {
            throw new NotFoundException("Question not found");
        }
        Course course = c.get();
        if (course.getUser().getId() != userContext.getUser().getId()) {
            throw new PermissionDeniedException();
        }

        Question question = new Question(questionDto);
        question.setCourse(course);
        questionDao.save(question);
    }

    public void deleteQuestion(Integer id) throws PermissionDeniedException, NotFoundException {
        Optional<Question> q = questionDao.findById(id);
        if (!q.isPresent()) {
            throw new NotFoundException("Question not found");
        }
        User u = q.get().getCourse().getUser();
        if (u == null) {
            throw new NotFoundException("User not found");
        }
        if (!u.getId().equals(userContext.getUser().getId())) {
            throw new PermissionDeniedException();
        }
        questionDao.deleteById(id);
    }

    public void updateQuestion(QuestionDto questionDto) throws PermissionDeniedException, NotFoundException {
        Optional<Question> q = questionDao.findById(questionDto.getId());
        if (!q.isPresent()) {
            throw new NotFoundException("Question not found");
        }
        Question question = q.get();
        User u = question.getCourse().getUser();
        if (u == null) {
            throw new NotFoundException("User not found");
        }
        if (!u.getId().equals(userContext.getUser().getId())) {
            throw new PermissionDeniedException();
        }
        question.setText(questionDto.getText());
        questionDao.save(question);
//        answerDao.deleteInBatchByQuestionId(question.getId());
//        question.updateFromDto(questionDto);
    }
}
