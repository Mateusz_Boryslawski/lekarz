package app.services;

import app.dao.TestmoduleDao;
import app.entities.Testmodule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TestmoduleService {
    @Autowired
    TestmoduleDao testmoduleDao;

    public void add(Testmodule testmodule) {
        testmoduleDao.save(testmodule);
    }

    public List<Testmodule> getAll() {
        return testmoduleDao.findAll();
    }

    public void remove(Testmodule testmodule) {
        testmoduleDao.delete(testmodule);
    }

    public Optional<Testmodule> get(Integer id) {
        return testmoduleDao.findById(id);
    }
}
