package app.services;

import app.dao.GroupDao;
import app.entities.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GroupService {

    @Autowired
    GroupDao groupDao;

    public void add(Group group) {
        groupDao.save(group);
    }

    public List<Group> getAll() {
        return groupDao.findAll();
    }

    public Optional<Group> findById(Integer id) {
        return groupDao.findById(id);
    }

    public void remove(Group group) {
        groupDao.delete(group);
    }
}