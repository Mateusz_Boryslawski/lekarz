package app.services;

import app.dao.UserDao;
import app.dto.Token;
import app.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Service
public class UserService {

    @Autowired
    UserDao userDao;

    @Autowired
    UserContext userContext;

    public void addNewUser(User user) {
        user.setToken(UUID.randomUUID().toString());
        userDao.save(user);
    }

    public Token login(User user) {
        Optional<User> userDB = userDao.findByEmail(user.getEmail());

        if(!userDB.isPresent()) {
            return null; // nie ma takiego usera
        } else if (user.getPassword().equals(userDB.get().getPassword())) {
            Token token = new Token();
            token.setToken(userDB.get().getToken());
            return token;
        } else { // bledne haslo
            return null;
        }
    }

    public User checkUserToken(Token token) {
        Optional<User> userDB = userDao.findByToken(token.getToken());

        if(userDB.isPresent()) {
            return userDB.get();
        } else {
            return null;
        }
    }

    public void logout() {
        Optional<User> userDB = userDao.findByEmail(userContext.getUser().getEmail());
        if(!userDB.isPresent()) {
            return;
        }
        userDB.get().setToken(UUID.randomUUID().toString());
    }
}