package app.services;

import app.dao.AnswerDao;
import app.dao.QuestionDao;
import app.entities.Answer;
import app.entities.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class QuestionService {

    @Autowired
    QuestionDao questionDao;

    public void add(Question question) {
        questionDao.save(question);
    }

    public List<Question> getAll() {
        return questionDao.findAll();
    }

    public Optional<Question> findById(Integer id) { return questionDao.findById(id); }

    public void remove(Question question) {
        questionDao.delete(question);
    }
}