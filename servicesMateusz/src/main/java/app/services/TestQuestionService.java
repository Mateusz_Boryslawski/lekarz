package app.services;

import app.dao.TestQuestionDao;
import app.entities.TestQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TestQuestionService {
    @Autowired
    TestQuestionDao testQuestionDao;

    public void add(TestQuestion testQuestion) {
        testQuestionDao.save(testQuestion);
    }
    public List<TestQuestion> getAll() {
        return testQuestionDao.findAll();
    }
    public void remove(TestQuestion testQuestion) {
        testQuestionDao.delete(testQuestion);
    }
    public Optional<TestQuestion> get(Integer id) {
        return testQuestionDao.findById(id);
    }
}