package app.services;

import app.dao.ResultDao;
import app.entities.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ResultService {

    @Autowired
    ResultDao resultDao;

    public void add(Result result) {
        resultDao.save(result);
    }

    public List<Result> getAll() {
        return resultDao.findAll();
    }

    public void remove(Result result) {
        resultDao.delete(result);
    }
}
