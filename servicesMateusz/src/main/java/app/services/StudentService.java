package app.services;

import app.dao.StudentDao;
import app.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StudentService {

    @Autowired
    StudentDao studentDao;

    public void add(Student student) {
        studentDao.save(student);
    }

    public List<Student> getAll() {
        return studentDao.findAll();
    }

    public Optional<Student> findByIndexNumber(Integer indexNumber) { return studentDao.findByIndexNumber(indexNumber); }

    public void remove(Student student) {
        studentDao.delete(student);
    }
}
