package app;

import app.entities.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

        public class QuestionSelector
        {
            private Course _course;
            public QuestionSelector(Course course)
            {
                _course = course;
            }

            public void getQuestions(Testmodule testmodule, int numberOfQuestions, int numberOfAnswers)
            {
                getQuestions(testmodule, numberOfQuestions, numberOfAnswers, false);
            }

            public void getQuestions(Testmodule testmodule, int numberOfQuestions, int numberOfAnswers, boolean isMultiAnswer)
            {
                Collection<Question> questions = _course.getQuestions();
                int numberOfCorrectAnswers = isMultiAnswer ? 2 : 1;
                questions = getQuestionsWithNOrMoreCorrectAnswers(questions, numberOfCorrectAnswers);
                questions = getQuestionsWithGivenNumberOfAnswers(questions, numberOfAnswers);   
                if(questions.size() > numberOfQuestions)
                    questions = ((List<Question>) questions).subList(0,numberOfQuestions);
                testmodule.setQuestions(toTestQuestions(testmodule, questions));
            }

            private List<Question> getQuestionsWithNOrMoreCorrectAnswers(Collection<Question> questions, int numberOfAnswers)
            {
                List<Question> multiQuestions = new ArrayList<>();
                for (Question question : questions)
                {
                    int correctAnswers = 0;
                    for (Answer answer : question.getAnswers())
                    {
                        if (answer.getIsCorrect())
                        {
                            correctAnswers++;
                        }
                    }
                    if (correctAnswers >= numberOfAnswers)
                    {
                        multiQuestions.add(question);
                    }
                }
                return multiQuestions;
            }

            private List<Question> getQuestionsWithGivenNumberOfAnswers(Collection<Question> questions, int numberOfAnswers)
            {
                List<Question> retQuestions = new ArrayList<>();
                for (Question question : questions)
                {
                    if (question.getAnswers().size() < numberOfAnswers)
                        continue;
                    else if (question.getAnswers().size() == numberOfAnswers)
                    {
                        retQuestions.add(question);
                    }
                    else
                    {
                        deleteUnnecessaryAnswers(question, numberOfAnswers);
                        retQuestions.add(question);
                    }
                }
                return retQuestions;
            }

            private void deleteUnnecessaryAnswers(Question question, int numberOfAnswers)
            {
                ArrayList<Answer> correctAnswers = new ArrayList<>();
                for (Answer answer : question.getAnswers())
                {
                    if (answer.getIsCorrect())
                    {
                        correctAnswers.add(answer);
                    }
                }
                if (correctAnswers.size() > numberOfAnswers)
                {
                    while (correctAnswers.size() > numberOfAnswers)
                    {
                        correctAnswers.remove(0);
                    }
                }
                else
                {
                    for (int i = 0; correctAnswers.size() < numberOfAnswers; ++i)
                    {
                        Answer answer = question.getAnswers().get(i);
                        if (!answer.getIsCorrect())
                        {
                            correctAnswers.add(answer);
                        }
                    }
                }
                question.setAnswers(correctAnswers);
            }

            private List<TestQuestion> toTestQuestions(Testmodule testmodule, Collection<Question> questions)
            {
                List<TestQuestion> testQuestions = new ArrayList<>();
                for (Question q : questions)
                {
                    TestQuestion testQuestion = new TestQuestion(q, testmodule);
                    testQuestions.add(testQuestion);
                }
                return testQuestions;
            }
        }
