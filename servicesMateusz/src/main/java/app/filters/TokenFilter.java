package app.filters;

import app.services.UserContext;
import app.dto.Token;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class TokenFilter implements Filter {
    @Autowired
    private UserService userService;
    @Autowired
    private UserContext userContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if (!httpServletRequest.getServletPath().equals("/user/login") &&
                !httpServletRequest.getServletPath().equals("/user/signUp")
                && !"OPTIONS".equals(httpServletRequest.getMethod())) {
            String token = httpServletRequest.getHeader("Authorization");
            if (token == null) { // token jest nieprawidlowy
                httpServletResponse.sendError(401, "Zły token");
            } else {
                Token token1 = new Token(token);
                if (userService.checkUserToken(token1) == null) {
                    httpServletResponse.sendError(401, "Nie znaleziono uzytkownika dla tokenu");
                } else {
                    userContext.setUser(userService.checkUserToken(token1));
                    filterChain.doFilter(servletRequest, servletResponse);

                }
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
            userContext.setUser(null);
        }
    }

    @Override
    public void destroy() {

    }
}