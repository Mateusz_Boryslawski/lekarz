package app.entities;

import app.dto.GroupDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "student_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String groupName;


    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "group",
            cascade = CascadeType.ALL)
    private Collection<Student> students = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "group",
            cascade = CascadeType.ALL)
    private Collection<Testmodule> testmodules = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "course_id")
    private Course course;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Collection<Student> getStudents() {
        return students;
    }

    public void setStudents(Collection<Student> students) {
        this.students = students;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public GroupDto getDto(){
        GroupDto dto = new GroupDto();
        dto.setGroupName(groupName);
        dto.setId(id);
        return dto;
    }
}
