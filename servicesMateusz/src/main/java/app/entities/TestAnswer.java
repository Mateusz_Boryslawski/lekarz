package app.entities;

import app.dto.AnswerDto;

import javax.persistence.*;

@Entity
public class TestAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String text;
    private boolean isCorrect;

    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "question_id")
    private TestQuestion question;

    public TestAnswer() {
    }

    public TestAnswer(AnswerDto answerDto) {
        text = answerDto.getText();
        isCorrect = answerDto.isCorrect();
    }

    public TestAnswer(Answer answer, TestQuestion testQuestion) {
        text = answer.getText();
        id = answer.getId();
        isCorrect = answer.getIsCorrect();
        question = testQuestion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean correct) {
        isCorrect = correct;
    }

    public TestQuestion getQuestion() {
        return question;
    }

    public void setQuestion(TestQuestion question) {
        this.question = question;
    }

    public String toString() {
        return text;
    }
}
