package app.entities;

import app.dto.CourseDto;
import app.dto.QuestionDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String courseName;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "course",
            cascade = CascadeType.ALL)
    private Collection<Group> groups = new ArrayList<Group>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "course",
            cascade = CascadeType.ALL)
    private Collection<Question> questions = new ArrayList<Question>();

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    public Course() {
    }

    public Course(CourseDto courseDto) {
        if (courseDto.getId() != null) {
            id = courseDto.getId();
        }
        courseName = courseDto.getCourseName();
    }

    public void updateFromDto(CourseDto courseDto) {
        courseName = courseDto.getCourseName();         // only the name is updated here
    }

    public CourseDto getDto() {
        CourseDto courseDto = new CourseDto();
        QuestionDto[] courseQuestions = new QuestionDto[questions.size()];

        int i = 0;
        for (Question q : questions) {
            courseQuestions[i++] = q.getDto();
        }
        courseDto.setCourseName(courseName);
        courseDto.setId(id);
        courseDto.setUserId(user.getId());
        courseDto.setCourseQuestions(courseQuestions);

        return courseDto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Collection<Group> getGroups() {
        return groups;
    }

    public void setGroups(Collection<Group> groups) {
        this.groups = groups;
    }

    public Collection<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }
}
