package app.entities;

import app.dto.AnswerDto;
import app.dto.QuestionDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String text;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "question",
            cascade = CascadeType.ALL)
    private List<Answer> answers;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "course_id")
    private Course course;

    public Question() {
    }

    public Question(QuestionDto questionDto) {
        updateFromDto(questionDto);
    }

    public void updateFromDto(QuestionDto questionDto) {
        text = questionDto.getText();

        answers = new ArrayList<Answer>();
        if (questionDto.getAnswers() != null) {
            for (AnswerDto a : questionDto.getAnswers()) {
                Answer ans = new Answer(a);
                ans.setQuestion(this);
                answers.add(ans);
            }
        }
    }

    public QuestionDto getDto() {
        QuestionDto questionDto = new QuestionDto();
        AnswerDto[] questionAnswers = new AnswerDto[answers.size()];

        int i = 0;
        for (Answer a : answers) {
            questionAnswers[i++] = a.getDto();
        }
        questionDto.setId(id);
        questionDto.setCourseId(course.getId());
        questionDto.setText(text);
        questionDto.setAnswers(questionAnswers);

        return questionDto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(text);
        sb.append(": \n");
        if (answers != null) {
            for (Answer a : answers) {
                sb.append(a.toString());
                sb.append(", \n");
            }
        }
        return sb.toString();
    }
}
