package app.entities;

import app.dto.AnswerDto;
import app.dto.QuestionDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TestQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String text;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "question",
            cascade = CascadeType.ALL)
    private List<TestAnswer> answers;

    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "testmodule_id")
    private Testmodule testmodule;

    public TestQuestion() {
    }

    public TestQuestion(QuestionDto questionDto) {      //TODO: is the creation of a new DTO justified?
        text = questionDto.getText();

        answers = new ArrayList<TestAnswer>();
        if (questionDto.getAnswers() != null) {
            for (AnswerDto a : questionDto.getAnswers()) {
                TestAnswer ans = new TestAnswer(a);
                ans.setQuestion(this);
                answers.add(ans);
            }
        }
    }

    public TestQuestion(Question question, Testmodule parentTestmodule) {
        text = question.getText();
        id = question.getId();
        answers = new ArrayList<>();
        for (Answer a : question.getAnswers()) {
            TestAnswer answer = new TestAnswer(a, this);
            answer.setText(a.getText());
            System.out.println(a.toString());
            answers.add(answer);
        }
        testmodule = parentTestmodule;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<TestAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<TestAnswer> answers) {
        this.answers = answers;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(text);
        sb.append(": \n");
        if (answers != null) {
            for (TestAnswer a : answers) {
                sb.append(a.toString());
                sb.append(", \n");
            }
        }
        return sb.toString();
    }
}
