package app.entities;

import app.dto.StudentDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private String lastName;
    private Integer indexNumber;

    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "student",
            cascade = CascadeType.ALL)
    private Collection<Result> results = new ArrayList<>();


    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "group_id")
    private Group group;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIndexNumber() {
        return indexNumber;
    }
    public void setIndexNumber(Integer indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Collection<Result> getResults() {
        return results;
    }
    public void setResults(Collection<Result> results) {
        this.results = results;
    }

    public Group getGroup() {
        return group;
    }
    public void setGroup(Group group) {
        this.group = group;
    }

    public StudentDto getDto()
    {
        StudentDto dto = new StudentDto();
        dto.setId(id);
        dto.setName(name);
        dto.setLastName(lastName);
        dto.setIndexNumber(indexNumber);
        dto.setResults(results.toArray(new Result[0]));
        return dto;
    }

}
