package app.entities;

import app.dto.AnswerDto;

import javax.persistence.*;

@Entity
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String text;
    private boolean isCorrect;

    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "question_id")
    private Question question;

    public Answer() {
    }

    public Answer(AnswerDto answerDto) {
        text = answerDto.getText();
        isCorrect = answerDto.isCorrect();
    }

    public AnswerDto getDto() {
        AnswerDto answerDto = new AnswerDto();
        answerDto.setIsCorrect(isCorrect);
        answerDto.setText(text);
        answerDto.setId(id);

        return answerDto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean correct) {
        isCorrect = correct;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String toString() {
        return text;
    }
}
