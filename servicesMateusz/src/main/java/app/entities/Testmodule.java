package app.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
public class Testmodule {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(fetch = FetchType.EAGER,
            mappedBy = "testmodule",
            cascade = {CascadeType.DETACH,
                    CascadeType.MERGE,
                    CascadeType.PERSIST,
                    CascadeType.REFRESH})
    private List<TestQuestion> questions = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "test",
            cascade = CascadeType.ALL)
    private Collection<Result> results = new ArrayList<>();


    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "group_id")
    private Group group;

    private String name;
    private Boolean usesNegativePoints;
    private Double thresholdFor3;
    private Double thresholdFor4;
    private Double thresholdFor5;

    public Double getThresholdFor3() {
        return thresholdFor3;
    }
    public void setThresholdFor3(Double thresholdFor3) {
        this.thresholdFor3 = thresholdFor3;
    }

    public Double getThresholdFor4() {
        return thresholdFor4;
    }
    public void setThresholdFor4(Double thresholdFor4) {
        this.thresholdFor4 = thresholdFor4;
    }

    public Double getThresholdFor5() {
        return thresholdFor5;
    }
    public void setThresholdFor5(Double thresholdFor5) {
        this.thresholdFor5 = thresholdFor5;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }
    public void setGroup(Group group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Boolean getUsesNegativePoints() {
        return usesNegativePoints;
    }
    public void setUsesNegativePoints(Boolean usesNegativePoints) {
        this.usesNegativePoints = usesNegativePoints;
    }

    public List<TestQuestion> getQuestions() {
        return questions;
    }
    public void setQuestions(List<TestQuestion> questions) {
        this.questions = questions;
    }

    public Collection<Result> getResults() {
        return results;
    }
}
