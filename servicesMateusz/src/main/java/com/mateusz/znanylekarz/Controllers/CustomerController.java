package com.mateusz.znanylekarz.Controllers;

import com.mateusz.znanylekarz.DTObjects.Token;
import com.mateusz.znanylekarz.entities.Customer;
import com.mateusz.znanylekarz.serviceInterfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/user")

public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping(path = "/signUp")
    public @ResponseBody
    ResponseEntity<Void> addNewUserController(@RequestBody Customer customer) {
        try {
            customerService.addNewCustomer(customer);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/login")
    public @ResponseBody
    ResponseEntity<Token> loginController(@RequestBody Customer customer) {
        try {
            return new ResponseEntity<Token>(customerService.login(customer), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Token>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/logout")
    public @ResponseBody
    ResponseEntity<Void> logoutController() {
        try {
            customerService.logout();
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
