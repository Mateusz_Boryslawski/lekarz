package com.mateusz.znanylekarz.Controllers;


import com.mateusz.znanylekarz.DTObjects.CustomerRegistrationDto;
import com.mateusz.znanylekarz.entities.Customer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class MainController {

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @PostMapping("/login")
    public String loginX(@ModelAttribute("customer") @Valid Customer customer,
                         BindingResult result) {
        return "loginsucces";
    }

    @GetMapping("/customer")
    public String userIndex() {
        return "customer/index";
    }
}