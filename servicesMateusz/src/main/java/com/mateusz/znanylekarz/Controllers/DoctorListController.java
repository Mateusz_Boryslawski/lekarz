package com.mateusz.znanylekarz.Controllers;

import com.mateusz.znanylekarz.DTObjects.CustomerRegistrationDto;
import com.mateusz.znanylekarz.serviceInterfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

public class DoctorListController {

    @Autowired
    private DoctorService doctorService;

    @ModelAttribute("doctor")
    public CustomerRegistrationDto userRegistrationDto() {
        return new CustomerRegistrationDto();
    }

    @GetMapping
    public String show(Model model) {
        return "registration";
    }

}
