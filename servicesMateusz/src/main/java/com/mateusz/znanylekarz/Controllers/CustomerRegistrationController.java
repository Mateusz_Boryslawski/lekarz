//package com.mateusz.znanylekarz.Controllers;
//
//
//import com.mateusz.znanylekarz.DTObjects.CustomerRegistrationDto;
//import com.mateusz.znanylekarz.entities.Customer;
//import com.mateusz.znanylekarz.serviceInterfaces.CustomerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.validation.BindingResult;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
//@Controller
//@RequestMapping("/registration")
//public class CustomerRegistrationController {
//
//    @Autowired
//    private CustomerService customerService;
//
//    @ModelAttribute("customer")
//    public CustomerRegistrationDto userRegistrationDto() {
//        return new CustomerRegistrationDto();
//    }
//
//    @GetMapping
//    public String showRegistrationForm(Model model) {
//        return "registration";
//    }
//
//    @PostMapping
//    public String registerUserAccount(@ModelAttribute("customer") @Valid CustomerRegistrationDto customerRegistrationDto,
//                                      BindingResult result){
//
//        Boolean existing = customerService.existsByEmail(customerRegistrationDto.getFirstName());
//
//        if (existing){
//            result.rejectValue("email", null, "There is already an account registered with that email");
//        }
//
//        if (result.hasErrors()){
//            return "registration";
//        }
//
//        customerService.save(customerRegistrationDto);
//        return "redirect:/registration?success";
//    }
//}
