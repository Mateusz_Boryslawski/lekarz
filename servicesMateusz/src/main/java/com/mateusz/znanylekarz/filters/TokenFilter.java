package com.mateusz.znanylekarz.filters;

import com.mateusz.znanylekarz.DTObjects.Token;
import com.mateusz.znanylekarz.serviceImplementations.CustomerContext;
import com.mateusz.znanylekarz.serviceInterfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class TokenFilter implements Filter {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerContext customerContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if (!httpServletRequest.getServletPath().equals("/login") &&
                !httpServletRequest.getServletPath().equals("/signUp")
                && !"OPTIONS".equals(httpServletRequest.getMethod())) {
            String token = httpServletRequest.getHeader("Authorization");
            if (token == null) { // token jest nieprawidlowy
                httpServletResponse.sendError(401, "Zły token");
            } else {
                Token token1 = new Token(token);
                if (customerService.checkCustomerToken(token1) == null) {
                    httpServletResponse.sendError(401, "Nie znaleziono uzytkownika dla tokenu");
                } else {
                    customerContext.setCustomer(customerService.checkCustomerToken(token1));
                    filterChain.doFilter(servletRequest, servletResponse);

                }
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
            customerContext.setCustomer(null);
        }
    }

    @Override
    public void destroy() {

    }
}