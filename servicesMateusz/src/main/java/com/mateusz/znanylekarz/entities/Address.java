package com.mateusz.znanylekarz.entities;

import com.mateusz.znanylekarz.entities.Enums.CityEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;


@Data
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String cityName;
    @Column
    private String street;
    @Column
    private String houseNumber;

    @OneToMany(fetch = FetchType.LAZY,
        mappedBy = "address",
        cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    private Collection<Doctor> doctors = new ArrayList<>();

    public Address() { }

    public Address(CityEnum cityEnum) {
        this.cityName = cityEnum.name();
    }

    public void addDoctor(Doctor doctor) {
        if(!this.doctors.contains(doctor))
            this.doctors.add(doctor);
    }

    public void removeDoctor(Doctor doctor) {
        if(this.doctors.contains(doctor))
            this.doctors.remove(doctor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(cityName, address.cityName) &&
                Objects.equals(street, address.street) &&
                Objects.equals(houseNumber, address.houseNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cityName, street, houseNumber);
    }
}
