//package com.mateusz.znanylekarz.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.util.Date;
//import java.util.Objects;
//
///**
// * Created by Michał on 2018-02-26.
// */
//
//
//@Data
//@Entity
//public class Visit {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @ManyToOne(cascade = {CascadeType.DETACH,
//            CascadeType.MERGE,
//            CascadeType.PERSIST,
//            CascadeType.REFRESH})
//    @JoinColumn(name = "doctor_id")
//    private Doctor doctor;
//
//    @ManyToOne(cascade = {CascadeType.DETACH,
//            CascadeType.MERGE,
//            CascadeType.PERSIST,
//            CascadeType.REFRESH})
//    @JoinColumn(name = "user_id")
//    private Customer customer;
//
//    @Column
//    private Date date;
//
//
//    public Visit(){}
//
//    public Visit(Doctor doctor, Customer customer) {
//        doctor.addVisit(this);
//        customer.addVisits(this);
//
//        this.doctor = doctor;
//        this.customer = customer;
//    }
//
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Visit)) return false;
//        Visit visit = (Visit) o;
//        return Objects.equals(doctor, visit.doctor) &&
//                Objects.equals(customer, visit.customer);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(doctor, customer);
//    }
//}
