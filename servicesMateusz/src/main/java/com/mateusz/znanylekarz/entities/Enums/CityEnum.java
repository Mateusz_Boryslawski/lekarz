package com.mateusz.znanylekarz.entities.Enums;

/**
 * Created by Michał on 2018-02-26.
 */
public enum CityEnum {
    WROCLAW,
    LODZ,
    KRAKOW,
    WARSZAWA,
    GDANSK,
    POZNAN,
    KATOWICE,
    LUBLIN,
    TORUN,
    SZCZECIN
}
