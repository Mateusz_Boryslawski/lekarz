//package com.mateusz.znanylekarz.entities;
//
//import lombok.Data;
//
//import javax.persistence.*;
//import java.util.Date;
//import java.util.Objects;
//
///**
// * Created by Michał on 2018-02-26.
// */
//
//@Data
//@Entity
//public class Visit_def {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    private Double price;
//    private Date date;
//
//    @ManyToOne(cascade = {CascadeType.DETACH,
//            CascadeType.MERGE,
//            CascadeType.PERSIST,
//            CascadeType.REFRESH})
//    @JoinColumn(name = "doctor_id")
//    private Doctor doctor;
//
//
//    public Visit_def() {}
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Visit_def)) return false;
//        Visit_def visit_def = (Visit_def) o;
//        return Objects.equals(price, visit_def.price) &&
//                Objects.equals(date, visit_def.date) &&
//                Objects.equals(doctor, visit_def.doctor);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(price, date, doctor);
//    }
//}
