package com.mateusz.znanylekarz.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;


@Data
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Email
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String token;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;


//    @OneToMany(fetch = FetchType.LAZY,
//            mappedBy = "customer",
//            cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    private Collection<Visit> visits = new ArrayList<>();

    public Customer() {}

    public Customer(String firstName) {
        this.firstName = firstName;
    }

//    public void addVisits(Visit visit) {
//        visit.setCustomer(this);
//        if(!visits.contains(visit))
//            this.visits.add(visit);
//    }
//
//    public void removeVisit(Visit visit) {
//        if(visits.contains(visit))
//            visits.remove(visit);
//    }

    public void addRole(Role role) {
        this.roles.add(role);
    }
}

