package com.mateusz.znanylekarz.entities;



import com.mateusz.znanylekarz.entities.Enums.SpecializationEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;


@Data
@Entity
public class Specialization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne(cascade = {CascadeType.DETACH,
                CascadeType.MERGE,
                CascadeType.PERSIST,
                CascadeType.REFRESH},
            fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;


    public Specialization(){}

    public void setName(SpecializationEnum specializationEnum) {
        this.name = specializationEnum.name();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Specialization)) return false;
        Specialization that = (Specialization) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(doctor, that.doctor);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, doctor);
    }
}
