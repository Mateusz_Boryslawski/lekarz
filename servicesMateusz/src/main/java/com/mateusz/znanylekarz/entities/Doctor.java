package com.mateusz.znanylekarz.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;


@Data
@Entity
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String name;
    @Column
    private String lastName;
    @Column
    private String email;
    @Column
    private String password;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "doctor",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Collection<Specialization> specializations = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "doctors_opinions",
            joinColumns = @JoinColumn(
                    name = "doctor_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "opinion_id", referencedColumnName = "id"))
    private Collection<Opinion> opinions = new ArrayList<>();

    @ManyToOne(cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "address_id")
    private Address address;

//    @OneToMany(fetch = FetchType.LAZY,
//            mappedBy = "doctor",
//            cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    private Collection<Visit_def> visit_defs = new ArrayList();

//    @OneToMany(fetch = FetchType.EAGER,
//            mappedBy = "doctor",
//            cascade = CascadeType.ALL,
//            orphanRemoval = true)
//    private Collection<Visit> visits = new ArrayList<>();


    public Doctor() {
    }

    public Doctor(String name) {
        this.name = name;
    }


    public void addSpecialization(Specialization specialization) {
        specialization.setDoctor(this);
        if (!specializations.contains(specialization))
            specializations.add(specialization);
    }

    public void removeSpecialization(Specialization specialization) {
        if (specializations.contains(specialization))
            this.specializations.remove(specialization);
    }


    public void setAddress(Address address) {

        if (this.address == null) {
            this.address = address;
            address.addDoctor(this);
        } else if (!this.address.equals(address)) {
            this.address.removeDoctor(this);
            if(this.address.getDoctors().isEmpty()) {
                // TUTAJ JAKOS USUNAC REKORD Z TABELI

            }
            this.address = address;
            if (this.address != null)
                address.addDoctor(this);
        }
    }

//    public void addVisit_defs(Visit_def visit_def) {
//        visit_def.setDoctor(this);
//        if (!visit_defs.contains(visit_def))
//            this.visit_defs.add(visit_def);
//    }
//
//    public void removeVisit_defs(Visit_def visit_def) {
//        if (visit_defs.contains(visit_def))
//            this.visit_defs.remove(visit_def);
//    }
//
//
//    public void addVisit(Visit visit) {
//        visit.setDoctor(this);
//        if (!visits.contains(visit))
//            this.visits.add(visit);
//    }
//
//    public void removeVisit(Visit visit) {
//        if (visits.contains(visit))
//            visits.remove(visit);
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(id, doctor.id) &&
                Objects.equals(name, doctor.name) &&
                Objects.equals(lastName, doctor.lastName) &&
                Objects.equals(email, doctor.email) &&
                Objects.equals(password, doctor.password) &&
                Objects.equals(specializations, doctor.specializations) &&
                Objects.equals(address, doctor.address);
//                Objects.equals(visit_defs, doctor.visit_defs) &&
//                Objects.equals(visits, doctor.visits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, email, password, specializations, address);
//        return Objects.hash(id, name, lastName, email, password, specializations, address, visit_defs, visits);
    }

}
