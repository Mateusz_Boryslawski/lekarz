package com.mateusz.znanylekarz.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Data
@Entity
public class Opinion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String opinion;


    public Opinion(){}

    public Opinion(String opinion) {
        this.opinion = opinion;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", opinion='" + opinion + '\'' +
                '}';
    }
}
