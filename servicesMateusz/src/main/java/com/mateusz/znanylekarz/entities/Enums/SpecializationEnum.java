package com.mateusz.znanylekarz.entities.Enums;

/**
 * Created by Michał on 2018-02-26.
 */

public enum SpecializationEnum {
    KARDIOLOG,
    CHIRURG,
    LARYNGOLOG,
    OKULISTA,
    PULMONOLOG,
    PEDIATRA,
    NEUROLOG,
    DERMATOLOG,
    STOMATOLOG,
    PSYCHOLOG,
    PSYCHIATRA,
    SEKSUOLOG,
    GINEKOLOG,
    UROLOG;

    SpecializationEnum() {
    }
}
