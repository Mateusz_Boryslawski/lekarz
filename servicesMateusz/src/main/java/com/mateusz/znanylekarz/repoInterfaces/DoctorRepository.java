package com.mateusz.znanylekarz.repoInterfaces;

import com.mateusz.znanylekarz.entities.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Transactional
@Repository
public interface DoctorRepository
    extends JpaRepository<Doctor, Long> {

    Doctor findByName(String name);

    Optional<Doctor> getById(Long id);

    Page<Doctor> findAllByName(String name, Pageable p);
    Page<Doctor> findAllByNameContaining(String name, Pageable p);
    Page<Doctor> findAllByLastNameContaining(String lastName, Pageable p);

    List<Doctor> findAll();

    Page<Doctor> findAllByAddress_CityNameAndSpecializations(String cityName, String specialization, Pageable p);


    Page<Doctor> findAllBySpecializations(String specialization, Pageable p);
}
