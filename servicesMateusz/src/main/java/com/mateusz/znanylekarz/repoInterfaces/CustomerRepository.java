package com.mateusz.znanylekarz.repoInterfaces;


import com.mateusz.znanylekarz.entities.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@Transactional
@Repository
public interface CustomerRepository
        extends JpaRepository<Customer, Long> {

    ArrayList<Customer> findAllByFirstName(String firstName);
    ArrayList<Customer> findAllByFirstNameContaining(String firstName);

    Page<Customer> findAllByFirstName(String name, Pageable p);
    Page<Customer> findAllByFirstNameContaining(String firstName, Pageable p);

    Optional<Customer> getById(Long id);

    Customer findByFirstName(String firstName);
    Optional<Customer> findByEmail(String email);
    // Customer findByEmail(String email);


    Boolean existsByFirstName(String firstName);
    Boolean existsByEmail(String email);

    Optional<Customer> findByToken(String token);

}
