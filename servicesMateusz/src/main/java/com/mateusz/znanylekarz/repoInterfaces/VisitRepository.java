//package com.mateusz.znanylekarz.repoInterfaces;
//
//import com.mateusz.znanylekarz.entities.Visit;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Optional;
//
//
//@Transactional
//@Repository
//public interface VisitRepository
//        extends JpaRepository<Visit, Long> {
//
//    Optional<Visit> getById(Long id);
//}
