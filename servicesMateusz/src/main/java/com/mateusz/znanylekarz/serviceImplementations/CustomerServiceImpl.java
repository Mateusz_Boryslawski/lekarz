package com.mateusz.znanylekarz.serviceImplementations;

import com.mateusz.znanylekarz.DTObjects.CustomerRegistrationDto;
import com.mateusz.znanylekarz.DTObjects.Token;
import com.mateusz.znanylekarz.entities.Customer;
import com.mateusz.znanylekarz.entities.Role;
import com.mateusz.znanylekarz.repoInterfaces.CustomerRepository;
import com.mateusz.znanylekarz.serviceInterfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    CustomerContext customerContext;

    @Override
    public Customer save(CustomerRegistrationDto customerRegistrationDto){
        Customer customer = new Customer();
        customer.setFirstName(customerRegistrationDto.getFirstName());
        customer.setLastName(customerRegistrationDto.getLastName());
        customer.setEmail(customerRegistrationDto.getEmail());
        customer.setPassword(passwordEncoder.encode(customerRegistrationDto.getPassword()));
        customer.setRoles(Arrays.asList(new Role("ROLE_USER")));
        return customerRepository.save(customer);
    }

//    @Override
//    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        Customer customer = customerRepository.findByEmail(email);
//        if (customer == null){
//            throw new UsernameNotFoundException("Invalid username or password.");
//        }
//        return new User(customer.getEmail(),
//                customer.getPassword(),
//                mapRolesToAuthorities(customer.getRoles()));
//    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }


    @Override
    public Customer update(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    @Override
    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }

    @Override
    public Long count() {
        return customerRepository.count();
    }

    @Override
    public ArrayList<Customer> findAllByFirstName(String firstName) {
        return customerRepository.findAllByFirstName(firstName);
    }

    @Override
    public ArrayList<Customer> findAllByFirstNameContaining(String firstName) {
        return customerRepository.findAllByFirstNameContaining(firstName);
    }

    @Override
    public Page<Customer> findAllByFirstName(String firstName, Pageable p) {
        return customerRepository.findAllByFirstName(firstName, p);
    }

    @Override
    public Page<Customer> findAllByFirstNameContaining(String firstName, Pageable p) {
        return customerRepository.findAllByFirstNameContaining(firstName, p);
    }

    @Override
    public Optional<Customer> getById(Long id) {
        return customerRepository.getById(id);
    }

    @Override
    public Boolean existsByFirstName(String firstName) {
        return customerRepository.existsByFirstName(firstName);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return customerRepository.existsByFirstName(email);
    }


    @Override
    public Optional<Customer> findByEmail(String email) {
        return customerRepository.findByEmail(email);
    }

    // ------------------

    @Override
    public Customer checkCustomerToken(Token token) {
        Optional<Customer> userDB = customerRepository.findByToken(token.getToken());

        if(userDB.isPresent()) {
            return userDB.get();
        } else {
            return null;
        }
    }

    @Override
    public void addNewCustomer(Customer customer) {
        customer.setToken(UUID.randomUUID().toString());
        customerRepository.save(customer);
    }

    @Override
    public Token login(Customer customer) {
        Optional<Customer> userDB = customerRepository.findByEmail(customer.getEmail());

        if(!userDB.isPresent()) {
            return null; // nie ma takiego usera
        } else if (customer.getPassword().equals(userDB.get().getPassword())) {
            Token token = new Token();
            token.setToken(userDB.get().getToken());
            return token;
        } else { // bledne haslo
            return null;
        }
    }

    @Override
    public void logout() {
        Optional<Customer> userDB = customerRepository.findByEmail(customerContext.getCustomer().getEmail());
        if(!userDB.isPresent()) {
            return;
        }
        userDB.get().setToken(UUID.randomUUID().toString());
    }
}
