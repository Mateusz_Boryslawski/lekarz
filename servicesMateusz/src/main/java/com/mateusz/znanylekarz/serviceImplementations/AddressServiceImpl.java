package com.mateusz.znanylekarz.serviceImplementations;


import com.mateusz.znanylekarz.entities.Address;
import com.mateusz.znanylekarz.entities.Doctor;
import com.mateusz.znanylekarz.repoInterfaces.AddressRepository;
import com.mateusz.znanylekarz.serviceInterfaces.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@Transactional
@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Override
    public Address checkIfAddressAlreadyExistInDB(Address address) {
        Optional<Address> optionalAddress = addressRepository
                .getByCityNameAndStreetAndHouseNumber(
                        address.getCityName(),
                        address.getStreet(),
                        address.getHouseNumber());
        return optionalAddress.orElse(address);
    }

    public Address test(Address address) {
        Optional<Address> optionalAddress = addressRepository
                .getAddressByCityName(
                        address.getCityName());

        return optionalAddress.orElse(address);
    }

    @Override
    public Optional<Address> getById(Long id) {
        return addressRepository.getById(id);
    }

    @Override
    public void delete(Address address) {

        ArrayList<Doctor> doctors = new ArrayList<>(address.getDoctors());
        doctors.forEach(s -> s.setAddress(null));
        addressRepository.delete(address);
    }
}
