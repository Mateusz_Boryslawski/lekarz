//package com.mateusz.znanylekarz.serviceImplementations;
//
//import com.mateusz.znanylekarz.entities.Visit;
//import com.mateusz.znanylekarz.repoInterfaces.VisitRepository;
//import com.mateusz.znanylekarz.serviceInterfaces.VisitService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Optional;
//
//@Transactional
//@Service
//public class VisitServiceImpl implements VisitService {
//
//    @Autowired
//    VisitRepository visitRepository;
//
//
//    @Override
//    public Visit save(Visit visit) {
//        return visitRepository.save(visit);
//    }
//
//    @Override
//    public Visit update(Visit visit) {
//        return visitRepository.save(visit);
//    }
//
//    @Override
//    public void delete(Visit visit) {
//
//        visit.getDoctor().removeVisit(visit);
//        visit.getCustomer().removeVisit(visit);
//
//        visit.setDoctor(null);
//        visit.setCustomer(null);
//
//        visitRepository.delete(visit);
//    }
//
//    @Override
//    public void deleteById(Long id) {
//        visitRepository.deleteById(id);
//    }
//
//    @Override
//    public Long count() {
//        return visitRepository.count();
//    }
//
//    @Override
//    public Optional<Visit> getById(Long id) {
//        return visitRepository.getById(id);
//    }
//}
