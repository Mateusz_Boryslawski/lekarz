package com.mateusz.znanylekarz.serviceImplementations;

import com.mateusz.znanylekarz.entities.Customer;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerContext extends ThreadLocal {
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}