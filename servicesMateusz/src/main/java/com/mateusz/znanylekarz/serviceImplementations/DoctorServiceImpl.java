package com.mateusz.znanylekarz.serviceImplementations;

import com.mateusz.znanylekarz.entities.Doctor;
import com.mateusz.znanylekarz.repoInterfaces.DoctorRepository;
import com.mateusz.znanylekarz.serviceInterfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;


    @Override
    public Doctor save(Doctor doctor){
        return doctorRepository.save(doctor);
    }

    @Override
    public Doctor update(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    @Override
    public void delete(Doctor doctor) {
        doctor.setAddress(null);
        doctor.getSpecializations().clear();
//        doctor.getVisits().clear();
        doctorRepository.delete(doctor);
    }

    @Override
    public void deleteById(Long id) {
        doctorRepository.deleteById(id);
    }

    @Override
    public Long count() {
        return doctorRepository.count();
    }

    @Override
    public Doctor findByName(String name) {
        return doctorRepository.findByName(name);
    }

    @Override
    public Optional<Doctor> getById(Long id) {
        return doctorRepository.getById(id);
    }

    @Override
    public Page<Doctor> findAllByName(String name, Pageable p) {
        return doctorRepository.findAllByName(name, p);
    }

    @Override
    public Page<Doctor> findAllByNameContaining(String name, Pageable p) {
        return doctorRepository.findAllByNameContaining(name, p);
    }

    @Override
    public List<Doctor> findAll(){
        return doctorRepository.findAll();
    }

    @Override
    public Page<Doctor> findAllByAddress_CityNameAndSpecializations(String cityName, String specialization, Pageable p) {
        return doctorRepository.findAllByAddress_CityNameAndSpecializations(cityName, specialization, p);
    }
}
