package com.mateusz.znanylekarz.DTObjects;

import com.mateusz.znanylekarz.entities.Address;
import com.mateusz.znanylekarz.entities.Specialization;

import java.util.ArrayList;
import java.util.Collection;

public class DoctorDto {

    private Long id;
    private String name;
    private String lastName;
    private Specialization specialization;
    private String city;
}
