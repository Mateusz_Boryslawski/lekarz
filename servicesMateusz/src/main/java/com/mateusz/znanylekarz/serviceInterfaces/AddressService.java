package com.mateusz.znanylekarz.serviceInterfaces;

import com.mateusz.znanylekarz.entities.Address;

import java.util.Optional;

public interface AddressService {

    Address checkIfAddressAlreadyExistInDB(Address address);

    Address test(Address address);


    //-----------------

    Optional<Address> getById(Long id);
    void delete(Address address);
}
