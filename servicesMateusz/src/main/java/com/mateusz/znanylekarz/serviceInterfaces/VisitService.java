//package com.mateusz.znanylekarz.serviceInterfaces;
//
//import com.mateusz.znanylekarz.entities.Visit;
//
//import java.util.Optional;
//
//public interface VisitService {
//
//    Visit save(Visit visit);
//    Visit update(Visit visit);
//    void delete(Visit visit);
//    void deleteById(Long id);
//    Long count();
//
//    Optional<Visit> getById(Long id);
//}
