package com.mateusz.znanylekarz.serviceInterfaces;

import com.mateusz.znanylekarz.DTObjects.CustomerRegistrationDto;
import com.mateusz.znanylekarz.DTObjects.Token;
import com.mateusz.znanylekarz.entities.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Optional;

public interface CustomerService {
    // public interface CustomerService extends UserDetailsService {


        Customer save(CustomerRegistrationDto customerRegistrationDto);
    Customer update(Customer customer);
    void delete(Customer customer);
    void deleteById(Long id);
    Long count();

    ArrayList<Customer> findAllByFirstName(String firstName);
    ArrayList<Customer> findAllByFirstNameContaining(String firstName);

    Page<Customer> findAllByFirstName(String firstName, Pageable p);
    Page<Customer> findAllByFirstNameContaining(String firstName, Pageable p);

    Optional<Customer> getById(Long id);

    Boolean existsByFirstName(String firstName);
    Boolean existsByEmail(String email);

    Optional<Customer> findByEmail(String email);

    // ---------

    void addNewCustomer(Customer customer);
    Token login(Customer customer);
    Customer checkCustomerToken(Token token);
    void logout();

}
