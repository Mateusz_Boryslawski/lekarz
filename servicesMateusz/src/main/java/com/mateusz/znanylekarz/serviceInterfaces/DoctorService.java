package com.mateusz.znanylekarz.serviceInterfaces;

import com.mateusz.znanylekarz.entities.Doctor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DoctorService {

    Doctor save(Doctor doctor);
    Doctor update(Doctor doctor);
    void delete(Doctor doctor);
    void deleteById(Long id);
    Long count();


    Optional<Doctor> getById(Long id);
    Doctor findByName(String name);

    Page<Doctor> findAllByName(String name, Pageable p);
    Page<Doctor> findAllByNameContaining(String name, Pageable p);


    List<Doctor> findAll();

    Page<Doctor> findAllByAddress_CityNameAndSpecializations(String cityName, String specialization, Pageable p);

}
