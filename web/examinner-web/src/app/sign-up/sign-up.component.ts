import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
// import { SignUpData } from '../model/signUpData';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServicesService} from '../services.service';
import {SignUpData} from '../model/signUpData';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  signUpData: SignUpData;

  constructor(private fb: FormBuilder,
              private router: Router,
              private service: ServicesService) {
    localStorage.setItem('token', null);
  }

  ngOnInit() {
    this.signUpForm = this.fb.group({
      email: [null, Validators.required],
      name: [null, Validators.required],
      lastName: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  signUp() {
    this.signUpData = this.signUpForm.value;
    this.service.signup(this.signUpData).subscribe();
    this.router.navigate(['login']);
  }

  login() {
    this.router.navigate(['login']);
  }
}
