import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Token} from './model/token';
import {User} from "./model/user";
import {EnumsDto} from "./model/enumsDto";
import {DoctorDto} from "./model/doctorDto";
import {OpinionDto} from "./model/opinionDto";


@Injectable()
export class ServicesService {

  url: string = 'http://localhost:8080';

  constructor(private http: HttpClient) {
  }

  login(user: User): Observable<Token> {
    return this.http.post<Token>(this.url + '/user/login', user);
  }

  logout(): Observable<any> {
    return this.http.post(this.url + '/user/logout', null);
  }

  signup(user: User): Observable<any> {
    return this.http.post(this.url + '/user/signUp', user);
  }

  getDoctors(enums: EnumsDto): Observable<any> {
    return this.http.post(this.url + '/doctor/getSpecificDoctors', enums);
  }

  doctorSignup(doctorDto: DoctorDto): Observable<any> {
    return this.http.post(this.url + '/doctor/signUp', doctorDto);
  }

  getAllDoctors():Observable<DoctorDto[]> {
    return this.http.get<DoctorDto[]>(this.url + '/doctor/getAllDoctors');
  }

  addOpinion(opinion: OpinionDto): Observable<any> {
    return this.http.post(this.url + '/user/addOpinion', opinion);
  }
}

