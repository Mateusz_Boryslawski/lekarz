import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TableModule} from 'primeng/table';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {MainComponent} from './main/main.component';
import {ChartModule, DataTableModule, DialogModule, DropdownModule, InputTextareaModule} from 'primeng/primeng';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'primeng/components/common/shared';
import {ServicesService} from './services.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthInterceptor} from './interceptors/auth.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LogoutComponent} from './logout/logout.component';
import {CheckboxModule} from 'primeng/checkbox';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {TabMenuModule} from 'primeng/tabmenu';
import {MenuTabComponent} from './menu-tab/menu-tab.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import {DataViewModule} from "primeng/dataview";
import { DoctorSignupComponent } from './doctor-signup/doctor-signup.component';
import { DoctorLoginComponent } from './doctor-login/doctor-login.component';

const routes: Routes = [
  // { path: 'add-question/:courseId', component: QuestionComponent },
  {path: 'login', component: LoginComponent},
  {path: 'doctorLogin', component: DoctorLoginComponent},
  {path: 'signUp', component: SignUpComponent},
  {path: 'doctorSignUp', component: DoctorSignupComponent},
  {path: 'main', component: MainComponent},
  {path: 'doctor-list', component: DoctorListComponent},
  {path: '', redirectTo: 'main', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    MainComponent,
    LogoutComponent,
    MenuTabComponent,
    DoctorListComponent,
    DoctorSignupComponent,
    DoctorLoginComponent,
  ],
  imports: [
    RouterModule.forRoot(
      routes,
      {enableTracing: false} // <-- debugging purposes only
    ),
    BrowserModule,
    NgbModule.forRoot(),
    CommonModule,
    SharedModule,
    DataTableModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    BrowserAnimationsModule,
    CheckboxModule,
    PdfViewerModule,
    TableModule,
    TabMenuModule,
    ChartModule,
    DropdownModule,
    DataViewModule,
    InputTextareaModule
  ],
  providers: [ServicesService, HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }],
  bootstrap: [AppComponent]
})

export class AppModule {
}
