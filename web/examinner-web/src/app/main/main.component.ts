import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ServicesService} from '../services.service';
import {SelectItem} from "primeng/api";
import {EnumsDto} from "../model/enumsDto";
import {DoctorDto} from "../model/doctorDto";
import {OpinionDto} from "../model/opinionDto";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  cities: SelectItem[];
  specialist: SelectItem[];

  selectedCity: String;
  selectedSpecialist: String;
  doctors: DoctorDto[];

  selectedDoctor: DoctorDto;
  displayInfoAboutDoctors: boolean;
  displayDialog: boolean;
  sortKey: string;
  sortOptions: SelectItem[];
  sortField: string;
  customerOpinion: string;
  sortOrder: number;
  doctorMark: number;
  isLoggedCustomer: boolean;

  constructor(private router: Router,
              private service: ServicesService) {


    this.cities = [
      {value: 'WROCLAW', label: 'Wrocław'},
      {value: 'LODZ', label: 'Lódź'},
      {value: 'KRAKOW', label: 'Kraków'},
      {value: 'WARSZAWA', label: 'Warszawa'},
      {value: 'GDANSK', label: 'Gdańsk'},
      {value: 'POZNAN', label: 'Poznań'},
      {value: 'KATOWICE', label: 'Katowice'},
      {value: 'LUBLIN', label: 'Lublin'},
      {value: 'TORUN', label: 'Toruń'},
      {value: 'SZCZECIN', label: 'Szczecin'}
    ];

    this.specialist = [
      {value: 'KARDIOLOG', label: 'Kardiolog'},
      {value: 'CHIRURG', label: 'Chirurg'},
      {value: 'LARYNGOLOG', label: 'Laryngolog'},
      {value: 'OKULISTA', label: 'Okulista'},
      {value: 'PULMONOLOG', label: 'Pulmonolog'},
      {value: 'PEDIATRA', label: 'Pediatra'},
      {value: 'NEUROLOG', label: 'Neurolog'},
      {value: 'DERMATOLOG', label: 'Dermatolog'},
      {value: 'STOMATOLOG', label: 'Stomatolog'},
      {value: 'PSYCHOLOG', label: 'Psycholog'},
      {value: 'PSYCHIATRA', label: 'Psychiatra'},
      {value: 'SEKSUOLOG', label: 'Seksulog'},
      {value: 'GINEKOLOG', label: 'Ginekolog'},
      {value: 'UROLOG', label: 'Urolog'}
    ];

    this.selectedCity = "WROCLAW";
    this.selectedSpecialist = "KARDIOLOG";

    this.sortOptions = [
      {label: 'Imię', value: 'name'},
      {label: 'Nazwisko', value: 'lastName'},
      {label: 'Email', value: 'email'},
      {label: 'Ocenie', value: 'mark'}
    ];
  }

  ngOnInit() {
    this.displayInfoAboutDoctors = false;
    this.isLoggedCustomer = localStorage.getItem("token") != null && localStorage.getItem("token") != "";

    // if(!this.isLoggedCustomer){
    //   localStorage.setItem("token", "")
    // }
  }

  getDoctors() {
    let e = new EnumsDto();
    e.specializationEnum = this.selectedSpecialist;
    e.cityEnum = this.selectedCity;
    this.service.getDoctors(e).subscribe(res => this.doctors = res);
  }

  findDoctors() {
    if (!this.isLoggedCustomer) {
      localStorage.setItem("token", "");
    }
    this.getDoctors();
    this.displayInfoAboutDoctors = true;
    if (!this.isLoggedCustomer) {
      localStorage.clear();
    }
  }

  loginAsDoctor() {
    this.router.navigate(['doctorLogin']);
  }

  login() {
    this.router.navigate(['login']);
  }

  selectDoctor(event: Event, doctor: DoctorDto) {
    this.selectedDoctor = doctor;
    console.log(doctor);
    this.displayDialog = true;
    event.preventDefault();
  }

  onDialogHide() {
    this.selectedDoctor = null;
  }

  onSortChange(event) {
    let value = event.value;

    this.sortOrder = 1;
    this.sortField = value;
  }

  sendOpinion(customerOpinion, selectedDoctor, doctorMark) {
    let op = new OpinionDto();
    op.mark = doctorMark;
    op.doctor = selectedDoctor;
    op.opinion = customerOpinion;
    console.log(this.isLoggedCustomer);
    this.service.addOpinion(op).subscribe();
    this.displayDialog = false;

  }

}
