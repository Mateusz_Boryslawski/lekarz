import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";

@Component({
  selector: 'app-menu-tab',
  templateUrl: './menu-tab.component.html',
  styleUrls: ['./menu-tab.component.css']
})

export class MenuTabComponent implements OnInit {

  constructor() { }
  private items: MenuItem[];

  ngOnInit() {
    this.items = [
      {label: 'Lekarze', icon: 'fa fa-fw fa-book', routerLink: ['/main']},
    ];
  }

}
