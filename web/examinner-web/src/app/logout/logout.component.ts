import {Component, OnInit} from '@angular/core';
import {ServicesService} from '../services.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private service: ServicesService,
              private router: Router) {
  }

  ngOnInit() {
  }

  logout() {
    this.service.logout().subscribe();
    localStorage.clear()
    this.router.navigate(['/']);
  }
}
