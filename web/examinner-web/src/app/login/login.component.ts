import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ServicesService} from '../services.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginData} from '../model/loginData';
import {User} from "../model/user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  user: User;

  constructor(private fb: FormBuilder,
              private router: Router,
              private service: ServicesService) {
    localStorage.setItem('token', null);
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    })
    localStorage.setItem("token", "");

  }

  login() {
    this.user = this.loginForm.value;
    console.log()
    // console.log(this.loginData)

    this.service.login(this.user).subscribe(
      res => {
        if (res.token != null) {
          localStorage.setItem('token', res.token);
          this.router.navigate(['main']);
        }
      }
    )
    // this.router.navigate(['main']);

  }

  signUp() {
    this.router.navigate(['signUp']);
  }
}
