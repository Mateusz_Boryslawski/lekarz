import {OpinionsDto} from "./opinionsDto";
import {AdresDto} from "./adresDto";

export class DoctorDto {
  opinions: OpinionsDto[]
  name: String
  lastName: String
  email: String
  adres: String
  id: number
  telephone: number
  mark: number
  address: AdresDto
  specialization: String

  constructor() {
  this.email = null;
  this.id = null;
  this.lastName = null;
  this.opinions = null;
  this.address = null;
  this.name = null;
  this.telephone = null;
  this.mark = null;
  this.adres = null;
  this.specialization = null;
  }
}
