import {DoctorDto} from "./doctorDto";

export class OpinionDto {
  doctor: DoctorDto
  opinion: String
  mark: number

  constructor() {
    this.doctor = null;
    this.opinion = null;
    this.mark = null;
  }
}
