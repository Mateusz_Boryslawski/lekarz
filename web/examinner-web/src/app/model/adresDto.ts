export class AdresDto {
  cityName: String
  street: String
  houseNumber: String

  constructor() {
    this.cityName = null;
    this.street = null;
    this.houseNumber = null;
  }
}
