import { Component, OnInit } from '@angular/core';
import {ServicesService} from "../services.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SignUpData} from "../model/signUpData";
import {User} from "../model/user";

@Component({
  selector: 'app-doctor-login',
  templateUrl: './doctor-login.component.html',
  styleUrls: ['./doctor-login.component.css']
})
export class DoctorLoginComponent implements OnInit {

  loginForm: FormGroup;
  user: User;

  constructor(private fb: FormBuilder,
              private router: Router,
              private service: ServicesService) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    })
    localStorage.setItem("token", "");

  }


  signUp(){
    this.router.navigate(['doctorSignUp']);
  }

  login(){

  }
}
