import { Component, OnInit } from '@angular/core';
import {ServicesService} from "../services.service";
import {Router} from "@angular/router";
import {SelectItem} from "primeng/api";
import {SignUpData} from "../model/signUpData";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DoctorDto} from "../model/doctorDto";
import {AdresDto} from "../model/adresDto";

@Component({
  selector: 'app-doctor-signup',
  templateUrl: './doctor-signup.component.html',
  styleUrls: ['./doctor-signup.component.css']
})
export class DoctorSignupComponent implements OnInit {
  cities: SelectItem[];
  specialist: SelectItem[];

  selectedCity: String;
  selectedSpecialist: String;
  street: String;
  houseNumber: String;

  signUpForm: FormGroup;
  doctorDto: DoctorDto;

  constructor(private fb: FormBuilder,
              private router: Router,
              private service: ServicesService) {
    this.signUpForm = this.fb.group({
      email: [null, Validators.required],
      name: [null, Validators.required],
      lastName: [null, Validators.required],
      street: [null, Validators.required],
      houseNumber: [null, Validators.required],
      telephone: [null, Validators.required],
      password: [null, Validators.required]
    });

    this.cities = [
      {value: 'WROCLAW', label: 'Wrocław'},
      {value: 'LODZ', label: 'Lódź'},
      {value: 'KRAKOW', label: 'Kraków'},
      {value: 'WARSZAWA', label: 'Warszawa'},
      {value: 'GDANSK', label: 'Gdańsk'},
      {value: 'POZNAN', label: 'Poznań'},
      {value: 'KATOWICE', label: 'Katowice'},
      {value: 'LUBLIN', label: 'Lublin'},
      {value: 'TORUN', label: 'Toruń'},
      {value: 'SZCZECIN', label: 'Szczecin'}
    ];

    this.specialist = [
      {value: 'KARDIOLOG', label: 'Kardiolog'},
      {value: 'CHIRURG', label: 'Chirurg'},
      {value: 'LARYNGOLOG', label: 'Laryngolog'},
      {value: 'OKULISTA', label: 'Okulista'},
      {value: 'PULMONOLOG', label: 'Pulmonolog'},
      {value: 'PEDIATRA', label: 'Pediatra'},
      {value: 'NEUROLOG', label: 'Neurolog'},
      {value: 'DERMATOLOG', label: 'Dermatolog'},
      {value: 'STOMATOLOG', label: 'Stomatolog'},
      {value: 'PSYCHOLOG', label: 'Psycholog'},
      {value: 'PSYCHIATRA', label: 'Psychiatra'},
      {value: 'SEKSUOLOG', label: 'Seksulog'},
      {value: 'GINEKOLOG', label: 'Ginekolog'},
      {value: 'UROLOG', label: 'Urolog'}
    ];

    this.selectedCity = "WROCLAW";
    this.selectedSpecialist = "KARDIOLOG";
  }
  ngOnInit() {
    localStorage.setItem("token", "");
  }

  login(){
    this.router.navigate(['doctorLogin']);
  }

    signUp() {
      // this.doctorDto = this.signUpForm.value;

      let doc = new DoctorDto();
      doc = this.signUpForm.value;
      doc.specialization = this.selectedSpecialist;
      doc.telephone = this.signUpForm.controls['telephone'].value;
      let adres = new AdresDto();
      adres.cityName = this.selectedCity;
      adres.street = this.signUpForm.controls['street'].value;
      adres.houseNumber = this.signUpForm.controls['houseNumber'].value;
      doc.address = adres;
      console.log(doc);
      this.service.doctorSignup(doc).subscribe();
      this.router.navigate(['doctorLogin']);
    }
}
